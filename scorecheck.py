import numpy as np
# generate file with $cat logs/<logname> | grep -a "Mean population sc" > score.txt
f = open("score.txt", "r")

lines = f.readlines()
numbs = []
for line in lines :
    temp = line.split(' ')
    temp = temp[-1].split("\x1b")
    temp = int(temp[0])
    numbs.append(temp)

arr = np.array(numbs)
arr.sort()
print(arr.shape)
print(np.median(arr))
print(arr.mean())
print(arr[0], arr[-1])
print(arr[-100:])
