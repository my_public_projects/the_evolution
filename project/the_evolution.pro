TEMPLATE = app
CONFIG += console
unix:QMAKE_CXXFLAGS_GNUCXX11 = -std=c++17
unix:QMAKE_CXXFLAGS_GNUCXX1Z = -std=c++17
win32: QMAKE_CXXFLAGS += /std:c++17
unix: QMAKE_CXXFLAGS += -std=c++17
unix: QMAKE_CXX = g++-8

TARGET = "the-evolution"

CONFIG -= app_bundle
CONFIG -= qt
QT -= core
QT -= gui
LIBS += -lsfml-graphics
LIBS += -lsfml-window
LIBS += -lsfml-system

CONFIG(debug, debug|release) {
    TARGET = $$TARGET"_d"
    message("DEBUG")
} else
{
    message("RELEASE")
    QMAKE_CXXFLAGS += -O3
}

DESTDIR = $$PWD/../bin

unix:{
    LIBS += -lpthread

defineReplace(copymFile_unix){
        SRC_PATH = $$shell_path($$1)
        SRC_FILES = $$shell_path($$2)
        DST_PATH = $$shell_path($$3)
        return ($$quote(cp -v $$escape_expand(\")$${SRC_PATH}$$escape_expand(\")$$SRC_FILES $$escape_expand(\")$${DST_PATH}$$escape_expand(\")$$escape_expand(\n\t)))
}

}


QMAKE_POST_LINK += $$quote(cp $$PWD/../consola.ttf $$DESTDIR/)
QMAKE_POST_LINK += $$quote(&& bash $$PWD/../makedirs.sh)

INCLUDEPATH += ../include

HEADERS += \
  ../include/bact_engine.h \
  ../include/bact_engine/bact.h \
  ../include/bact_engine/food_generator.h \
  ../include/bact_engine/leaf.h \
  ../include/bact_engine/neuro_bact.h \
  ../include/bact_engine/particles.h \
  ../include/bact_engine/poper.h \
  ../include/bact_engine/population_controller.h \
  ../include/defines.h \
  ../include/bact_engine/engine.h \
  ../include/bact_engine/object.h \
  ../include/logger.h \
  ../include/multithreading/queue.h \
  ../include/multithreading/thread_pool.h \
  ../include/neuro_engine/ann.h \
  ../include/neuro_engine/neuro_utility.h \
  ../include/neuro_engine/neurone.h \
  ../include/render/render.h \
  ../include/statistics/param_value_set.h \
  ../include/statistics/stat_frame.h \
  ../include/statistics/stat_writer.h \
  ../include/statistics/statistic_holder.h \
  ../include/terminal.h


SOURCES += ../src/main.cpp \
  ../src/bact_engine/bact.cpp \
  ../src/bact_engine/engine.cpp \
  ../src/bact_engine/food_generator.cpp \
  ../src/bact_engine/leaf.cpp \
  ../src/bact_engine/neuro_bact.cpp \
  ../src/bact_engine/object.cpp \
  ../src/bact_engine/particles.cpp \
  ../src/bact_engine/poper.cpp \
  ../src/bact_engine/population_controller.cpp \
  ../src/defines.cpp \
  ../src/logger.cpp \
  ../src/multithreading/thread_pool.cpp \
  ../src/neruo_engine/ann.cpp \
  ../src/neruo_engine/neurone.cpp \
  ../src/statistics/param_value_set.cpp \
  ../src/statistics/stat_frame.cpp \
  ../src/statistics/stat_writer.cpp \
  ../src/statistics/statistic_holder.cpp \
  ../src/terminal.cpp


