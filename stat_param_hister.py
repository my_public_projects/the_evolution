import sys
import matplotlib.pyplot as plt
import numpy as np

if(len(sys.argv) < 3) :
	print("\033[31mERROR: no file path or param name in args\n\033[0m")
	exit()
file_name = sys.argv[1]

params = sys.argv[2:]

for param_name in params :

	#param_name = sys.argv[2]

	print(file_name)
	file = open(file_name, 'r')

	lines = file.readlines()


	frames = []
	for line in lines :
		pos = line.find(param_name)
		if(pos!=-1) : 
			frames.append(line);

	print(param_name+ " frames", len(frames))



#259;generation_num:0;hi_score:253;life_time:259;low_score:-3;mean_life_time:0;mutation_delta:0.1;

	values = []
	for frame in frames :
		params = frame.split(";")
		for param in params :
			pos = param.find(param_name)
			if(pos != -1) :
				temp = param.split(":")
				values.append(float(temp[1]))
				break
	

	print(param_name + " values", len(values))

	print("mean", np.array(values).mean())

	plt.hist(values, bins=100)




#plt.savefig("./" + file_name[:-4] + "png")
plt.show()


#mean_life_time
