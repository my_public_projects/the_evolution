import sys
import matplotlib.pyplot as plt
import numpy as np

if(len(sys.argv) < 2) :
	print("\033[31mERROR: no population file path in args\n\033[0m")
	exit()
file_name = sys.argv[1]

print(file_name)
file = open(file_name, 'r')

lines = file.readlines()

print("lines", len(lines))

main_line = lines[4]

ANNS = main_line.split("/")

ANNS = ANNS[:len(ANNS)-1]

print("ANNS", len(ANNS))
#print(ANNS[29])

parsed_anns = []

for neurone in ANNS :
	temp = neurone.split("*")
	temp = temp[:len(temp)-1]
	print("nerurones", len(temp))
	parsed_anns.append(temp)

ANNS = parsed_anns


print("ANNS", len(ANNS))
print(ANNS[0][0])

w_anns = []

for ann in ANNS:
	w_ann = []
	for neurone in ann :
		ws = neurone.split(";")[1:]
		ws = ws[2:3:2]
		if(len(ws) == 0):
			break
		ws = float(ws[0])
		print(ws)

		w_anns.append(ws)






plt.hist(w_anns, bins=500)
plt.savefig("./" + file_name[:-3] + "png")
plt.show()


#mean_life_time
