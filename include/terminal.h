#ifndef TERMINAL_H
#define TERMINAL_H
#include <iostream>
#include <thread>
#include <functional>
#include <map>
#include <assert.h>

#include "logger.h"
#include "multithreading/queue.h"




struct comm_message
{
    using ptr = std::shared_ptr<comm_message>;
    std::string command_line;
    int key;
    comm_message(int _key, const std::string line)
        :command_line(line),
         key(_key)
    {

    }


};

/*!
 * \brief The command class holds functor which will been executed when command
 * entered
 */
class command {
    /*!
     * \brief m_name - command
     */
    std::string m_name;

    /*!
     * \brief m_key identifer of command for fast handling
     */
    int m_key ;
    /*!
     * \brief m_descr - description if enter "help" command then it will showed
     */
    std::string m_descr = "No_description";


    /*!
     *  \brief m_data - receiver's thread safe queue for pushing command
     */
    std::shared_ptr<multithreading::queue<comm_message::ptr>> m_data;


public:
    command(){}

    command(const std::string & name,
            int key,
            const std::string & descript,
            const std::shared_ptr<multithreading::queue<comm_message::ptr>> &
            receiver_queue)
        : m_name(name),
          m_key(key),
          m_descr(descript),
          m_data(receiver_queue)

    {

    }

    /*!
     * \brief The run method send commandline to receiver
     * \param line - full command string like "load_p ../autosaves/popul0.pop"
     */
    void run(const std::string & line)
    {
        m_data->push(std::make_shared<comm_message>(m_key, line));
    }

    /*!
     * \brief The name method gives access to command name
     * \return string name of command
     */
    const std::string & name() const
    {
        return m_name;
    }

    /*!
     * \brief The descr method gives access to command decription
     * \return
     */
    const std::string & descr() const
    {
        return m_descr;
    }

    /*!
     * \brief The setDescr method sets command descrioption
     * \param descr - description of command
     */
    void setDescr(const std::string &descr)
    {
        m_descr = descr;
    }
};


/*!
 * \brief The terminal class command console interface
 */
class terminal
{

    /*!
     * \brief m_commands - map of available commands
     */
    std::map<std::string, command> m_commands;

    std::shared_ptr<multithreading::queue<comm_message::ptr>> m_commands_queue;

    /*!
     * \brief m_is_run - flag for stroping thread
     */
    bool m_is_run = false;

    /*!
     * \brief m_thread - in this thread reading from input occures
     */
    std::thread m_thread;

    /*!
     * \brief m_commands_mutex - mutex for m_commands map be thread safe
     */
    std::mutex m_commands_mutex;

    /*!
     * \brief The run_command method finds and executes entered command
     * \param command - full command string like
     * "load_p ../autosaves/popul0.pop"
     */
    void run_command(const std::string & command);


    /*!
     * \brief The help method prints list of commands
     */
    void help();


    /*!
     * \brief The start method runs in m_thread. Reads input and runs commands
     */
    void start();


    terminal();

    terminal(const terminal &) = delete ;
    terminal(terminal&&) = delete ;
    terminal & operator=(const terminal&) = delete ;
    terminal & operator=(terminal&&) = delete ;


    ~terminal();

    static terminal & instance();


    void handle_commands();


    /*!
     * \brief The run_thread method runs terminal thread.
     * \return
     */
    bool run_thread();

    /*!
     * \brief The stop_thread method stops terminal thread
     */
    void stop_thread();

    /*!
     * \brief The new_command method adds new command to terminal
     * (terminal must been runed)
     * \param comm - new command
     */
    bool new_command( const command & comm);


    /*!
     * \brief The rem_command method removes command from handling
     * \param name - command name
     * \return 1 if command removed
     */
    size_t rem_command(const std::string& name);

public:


    /*!
     * \brief The add_command method is interface for adding custom commands
     * \param comm - custom command
     * \return true if command was been added
     */
    static bool add_command(const command & comm);

    /*!
     * \brief The remove_command method is interface for removing command
     * \param comm_name - command name
     * \return true if command was removed
     */
    static bool remove_command(const std::string & comm_name);




};

#endif // TERMINAL_H

