#ifndef DEFINES_H
#define DEFINES_H
#include <cstdlib>
#include <random>
#include <queue>
#include <functional>
#include <memory>
#include <mutex>

#include <SFML/Graphics.hpp>


using ull = unsigned long long int;
using lli = long long int;


static const sf::Font g_font =
        [](){ sf::Font res;  res.loadFromFile("consola.ttf"); return res;}();



/*!
 * \brief The object_type enum for avoid RTTI
 */
enum class object_type : int
{
    object,
    leaf,
    bact
};

/*!
 * \brief The physic_type enum uses in neuro_bact.cpp at line: 174
 */
enum class physic_type : int
{
    teleport,
    speed,
    force
};


/*!
 * \brief The slice function splices std::string by char separator
 * \param [out] out - result vector of strings
 * \param [in] in - string for splicing
 * \param separator - separator symbol
 */
void slice(std::vector<std::string> & out, const std::string & in, char separator);

/*!
 * \brief The sqare_distance function calculates sqared distance between two points
 * \param x1 - X coordinate of first point
 * \param y1 - Y coordinate of first point
 * \param x2 - X coordinate of second point
 * \param y2 - Y coordinate of second point
 * \return squared distance
 */
inline float sqare_distance(float x1, float y1, float x2, float y2)
{
    float x = std::fabs(x1 - x2);
    x *= x;
    float y = std::fabs(y1 -y2);
    y *= y;
    return  x + y;
}

/*!
 * \brief The distance function calculates distance between two points
 * \param x1 - X coordinate of first point
 * \param y1 - Y coordinate of first point
 * \param x2 - X coordinate of second point
 * \param y2 - Y coordinate of second point
 * \return distance
 */
inline float distance(float x1, float y1, float x2, float y2)
{
    return std::sqrt(sqare_distance(x1, y1 , x2, y2));
}

/*!
 * \brief The get_time function
 * \return current time in string format
 */
std::string get_time();


/*!
 * \brief g_density - global parametr of materia density
 */
const float g_density = 0.8f;

/*!
 * \brief g_pi - PI constant
 */
const float g_pi = 3.14159265f;

/*!
 * \brief g_world_width - world width
 */
const float g_world_width = 1920.0f;

/*!
 * \brief g_world_height - world height
 */
const float g_world_height = 960.0f;

/*!
 * \brief g_friction - negative acceleration
 */
const float g_friction = 0.99f;

/*!
 * \brief g_EPS - epsilon for compare double and float
 */
const float g_EPS = 1e-4f;

/*!
 * \brief g_physic_type -
 */
const physic_type g_physic_type = physic_type::force;

/*!
 * \brief g_leaf_number - number of food
 */
const ull   g_leaf_number = 50;

/*!
 * \brief g_leaf_mass_burn_out - delta of mass of leaf which burn out per frame
 */
const float g_leaf_mass_burn_out = -0.05f;

/*!
 * \brief g_leaf_max_born_mass - max mass of leaf in respawn time
 */
const float g_leaf_max_born_mass = 200;

/*!
 * \brief g_leaf_min_born_mass - min mass of leaf in respawn time
 */
const float g_leaf_min_born_mass = 30;

/*!
 * \brief g_leaf_death_mass - minimal necessary mass for live of leaf. After
 * this value leaf will be respawned
 */
const float g_leaf_death_mass = 15;

/*!
 * \brief g_view_dist - length of bact's sensors
 */
static const float g_view_dist = 150;

/*!
 * \brief g_max_force - upper limit of thrust
 */
const float g_max_force = 5.0f;

/*!
 * \brief g_bact_mass_burn_out_idle - max delta of mass of bact which burn out
 * when thrusting
 */
const float g_bact_mass_burn_out_idle = -0.1f;

/*!
 * \brief g_bact_mass_burn_out -  delta of mass of bact which burn out per frame
 */
const float g_bact_mass_burn_out = -0.15f;

/*!
 * \brief g_bact_born_mass - default mass with which bacts will created
 */
const float g_bact_born_mass = 100;


const float g_bact_born_energy = 200;

const float g_bact_baby_energy = 500;

const float g_bact_division_energy = 400;

/*!
 * \brief g_bact_death_mass -  bacts with value of mass lower that value will die
 */
const float g_bact_death_mass = 10;

/*!
 * \brief g_bact_max_mass - maximal mass of a bact. If bact overgrows that value
 *  the bact will explode
 */
const float g_bact_max_mass = 5000;

const size_t g_bact_max_age = 10000;

const size_t g_age_of_adult = 2000;

/*!
 * \brief g_paricle_live - number of frames which particles will live
 */
const int g_paricle_live = 25;


const double g_radiation_value = 3.0593;
/*!
 * \brief g_radiation_freq - mutation delta multiples to g_radiation_value
 *  for a population
 * each \a g_radiation_freq populations
 */
const size_t g_radiation_freq = 340;


/*!
 * \brief g_helheim_seleection_freq - every g_helheim_seleection_freq
 * populations will be loaded from the helheim storage and the storage
 * will be cleaned
 */
const size_t g_helheim_seleection_freq = 50;


static_assert (g_radiation_freq % g_helheim_seleection_freq,
               "(g_radiation_freq % g_helheim_seleection_freq)"
               " must return not 0 value");




#endif // DEFINES_H
