#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <thread>
#include <functional>
#include <atomic>
#include <iostream>

#include "multithreading/queue.h"
namespace multithreading  {

class thread_pool
{
    using job = std::shared_ptr<std::function<void()>>;

    size_t m_threads_count;
    std::atomic_bool m_is_working;
    std::atomic_bool m_intense;

    queue<job> m_jobs_q;
    std::vector<std::thread> m_threads;


    void worker();


    void stop_threads();

public:

    thread_pool(
            size_t thread_count = 0,
            bool intense = false,
            size_t max_queue_size = 0);

    ~thread_pool();


    bool submit(const std::function<void()> & job);

    bool submit(std::function<void()> && job);

    bool submit(const job & job);

    size_t get_threads_count() const;

    size_t get_max_queue_size() const;

};

}//namespace multithreading
#endif // THREAD_POOL_H
