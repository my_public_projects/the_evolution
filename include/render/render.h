#ifndef RENDER_H
#define RENDER_H
#include "defines.h"
#include "bact_engine/object.h"
#include <SFML/Graphics.hpp>
#include <mutex>
#include <thread>

/*!
 * \brief The render class controls rendering window
 */
class render
{
    sf::RenderWindow m_window;

    std::mutex m_mutex;



public:
    /*!
     * \brief render - constructs render with new render window
     */
    render()
//        : m_window(sf::VideoMode(static_cast<int>(g_world_width),
//                                 static_cast<int>(g_world_height)), "THE EVOLUTION")
    {
        //show();
       // close();
    }


    ~render()
    {
        m_window.close();
    }

    /*!
     * \brief The showed method
     * \return true if the render window is shown, false otherwise
     */
    inline bool showed() const
    {
        return m_window.isOpen();
    }

    /*!
     * \brief The show method shows the render window
     */
    inline void show()
    {
        std::lock_guard<std::mutex> lk(m_mutex);
        if(m_window.isOpen())
        {
            return;
        }
        m_window.create(sf::VideoMode(static_cast<int>(g_world_width),
                                      static_cast<int>(g_world_height)), "THE EVOLUTION");
        m_window.setFramerateLimit(60);
    }

    /*!
     * \brief The close method closes the render window
     */
    inline void close()
    {
        std::lock_guard<std::mutex> lk(m_mutex);
        if(!m_window.isOpen())
        {
            return;
        }
        m_window.close();

    }

    /*!
     * \brief The draw method draws the scene's objects
     * \param scene - vector of scene's objects
     */
    void draw(const std::vector<b_engine::object_ptr> & scene)
    {
        std::lock_guard<std::mutex> lk(m_mutex);
        if(!m_window.isOpen())
        {
            return;
        }
        m_window.clear();
        for(const b_engine::object_ptr & o: scene)
        {
            if(!o)
            {
                continue;
            }
            o->draw(m_window);
        }
        m_window.display();
        sf::Event e;
        while (m_window.pollEvent(e)) {
            if(e.type == sf::Event::Closed)
            {
                m_window.close();
            }
        }
        //std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
};

#endif // RENDER_H
