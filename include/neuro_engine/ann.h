#ifndef NETWORK_H
#define NETWORK_H
#include <vector>
#include "neurone.h"
namespace n_engine {
using layer = std::vector<neurone>;


/*!
 * \brief The layer_param struct represents a structure of the layer of ANN
 */
struct layer_param
{
    /*!
     * \brief The size member is a count of neurons in layer
     */
    ulli size;

    /*!
     * \brief The type member is a activation type of neurons
     */
    activation_type type;

    /*!
     * \brief layer_param - constucts layer param
     * \param _size - size of layer
     * \param _type -
     */
    layer_param(ulli _size, activation_type _type = activation_type::sigmoid)
        : size(_size),
          type(_type)
    {
    }
};

/*!
 * \brief The ann class is a representation of artifical neural network
 */
class ann {

    /*!
     * \brief m_layers - vector of ANN layers
     */
    std::vector<layer> m_layers;


    std::vector<layer>::iterator m_hidden_first;
    std::vector<layer>::iterator m_hidden_last;

    /*!
     * \brief m_reccurent - if true then ANN is reccurent propogation
     * else ANN will be is reccurent
     */
    bool m_reccurent;

    /*!
     * \brief The init method initializes ANN's links with random weights
     */
    void init();

public:

    /*!
     * \brief ann - constructs ANN by input params
     * \param layers_params - params of layers
     * \param reccurent - type of propogation
     */
    ann(const std::vector<layer_param> & layers_params,
        bool reccurent = false);

    /*!
    * \brief The layers method gives access to layers of ANN
    * \return reference to vector of layers
    */
   std::vector<layer> &layers();

   /*!
    * \brief The set_input method sets input signal to ANN
    * \details if size of inputs vector is  not eqal  size of zero layer assert
    * will be throwed
    * \param inputs - vector of double type with input values
    */
   void set_input(const std::vector<double> & inputs);

   /*!
    * \brief The process method calcs ANN and sets output signals of ANN
    */
   void process();

   /*!
    * \brief The get_output method fills vector \a out with outputs values
    * \param [out] out - reffernce to vector for fill output signals
    */
   void get_output(std::vector<double> & out);

   /*!
    * \brief The copy method copies the ANN from other ANN
    * \param r - ANN for copying
    */
   void copy(const ann & r);

   void brain_fuck(const ann & parent_one, const ann & parent_two);

   /*!
    * \brief The mutate_weights method mutate weights of ANN
    * \param percent - percent of a neurone's links which will be corrected
    * \param delta - value of range for weight corecting. Real range in
    * [-delta ; delta]
    */
   void mutate_weights(ulli percent, double delta);


   /*!
    * \brief mutate_struct to the future
    * \param percent
    * \param opt_param
    */
   void mutate_struct(ulli percent, double opt_param);

   /*!
    * \brief The serialize method serialize state of ANN to string
    * \param state - string for saving
    */
   void serialize(std::string & state) const;

   /*!
    * \brief The deserialize method load state of ANN from string
    * \param state - string for deserialization
    */
   void deserialize(const std::string &state);
};
}//namespace n_engine
#endif // NETWORK_H
