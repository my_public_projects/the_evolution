#ifndef NEURONE_H
#define NEURONE_H
#include "neuro_utility.h"
#include <vector>

namespace n_engine
{
class ann;
/*!
 * \brief The link struct represents a neural link between neurons
 */
struct link
{
    /*!
     * \brief The m_out_neurone_layer - index of layer
     */
    ulli m_out_neurone_layer;

    /*!
     * \brief The m_out_neurone_index - index of receiver neurone
     */
    ulli m_out_neurone_index;

    /*!
     * \brief The m_weight - weight of link
     */
    double m_weight;

    /*!
     * \brief link - constructs new link
     * \param layer - index of layer with reciver neuron
     * \param index - index of receiver neuron in layer
     * \param random - if true then weight will be random initializated by range
     *  [-1:1] else it will be initializated by zero value
     */
    link(ulli layer, ulli index, bool random)
        : m_out_neurone_layer(layer),
          m_out_neurone_index(index)
    {
        if(random) m_weight = static_cast<double>(rand() % 2000 - 1000) / 1000.0;
        else m_weight = 0.0;
    }

    /*!
     * \brief The mutate method adds delta to weight
     * \param delta - delta of mutation
     */
    inline void mutate(double delta)
    {
        m_weight += delta;
        if(m_weight > 1) m_weight = 1;
        else if(m_weight < -1) m_weight = -1;
    }

    /*!
     * \brief operator == - compare operator
     * \param r - value for compare
     * \return true if all indexes is match and absolute value of difference of
     * weights lower 1e-4 otherwise false
     */
    bool operator == (const link & r) const
    {
        return m_out_neurone_index == r.m_out_neurone_index &&
                m_out_neurone_layer == r.m_out_neurone_layer &&
                std::fabs(m_weight - r.m_weight) < 1e-4;
    }

};


/*!
 * \brief The neurone class representates a neuron of artiffical neuron network
 */
class neurone
{
    friend class ann;
    /*!
     * \brief m_value output value
     */
    double m_value = 0.0;

    /*!
     * \brief m_input - input value
     */
    double m_input = 0.0;

    /*!
     * \brief m_index - index of neuron in layer
     */
    ulli m_index;

    /*!
     * \brief m_layer - index of layer
     */
    ulli m_layer;

    /*!
     * \brief m_type - type of activation
     */
    activation_type m_type = activation_type::sigmoid;

    /*!
     * \brief m_ann - pointer to ANN which onwns the neuron
     */
    ann * m_ann = nullptr;

    /*!
     * \brief m_reccurent - type of propagation
     */
    bool m_reccurent;


    std::vector<link> m_links;

    /*!
     * \brief The links_forward method creates \a number links for
     * forward propagation
     * \param number - count of links
     */
    void links_forward(ulli number);

    /*!
     * \brief  links_recurent - this method creates \a number links to random
     * layer and random neuron
     * \param number - count of links
     */
    void links_recurent(ulli number);

    /*!
     * \brief The propagation method propogates weighted output value by links
     */
    void propagation();

public:


    /*!
     * \brief neurone - creates neurone instance
     * \param network - pointer to owner ANN
     * \param prop_type - propagation type
     * \param index - index of neuron in layer
     * \param layer - index of layer in ann
     * \param type - activation type
     */
    neurone(ann * network, bool prop_type,
            ulli index, ulli layer,
            activation_type type = activation_type::sigmoid);


    /*!
     * \brief  operator == compares neurones
     * \param r - neurone for compare
     * \return true if all links is equal
     */
    bool operator == (const neurone & r) const;

    /*!
     * \brief The init_links method is initializates links in subjection from
     * propagation type(links_forward/links_recurent)
     * \param number - count of links
     */
    void init_links(ulli number);

    /*!
     * \brief The activate method calcs the output value and calls the
     * propagation method
     */
    void activate();

    /*!
     * \brief The reccurent_propagation method
     */
    void reccurent_propagation();

    /*!
     * \brief The inc_input method adds the \a xw(weighted output value of other
     *  neurone) to the input value. This method calling neuron with link to
     * current neuron
     * \param xw - weighted output value of other neurone
     */
    void inc_input(double xw);

    /*!
     * \brief The type method
     * \return activation type of neurone
     */
    inline activation_type type() const
    {
        return m_type;
    }

    /*!
     * \brief The set_type method sets an activation type
     * \param type - new activation type
     */
    void set_type(const activation_type &type);

    /*!
     * \brief The value method
     * \return output value
     */
    inline double value() const
    {
        return m_value;
    }

    /*!
     * \brief The index method
     * \return index of neurone in layer
     */
    inline ulli index() const
    {
        return m_index;
    }

    /*!
     * \brief The layer method
     * \return index of layer in ANN
     */
    inline ulli layer() const
    {
        return m_layer;
    }

    /*!
     * \brief The links method gives access to links of neurone
     * \return const refference to vector of links
     */
    const std::vector<link> & links() const;

    /*!
     * \brief The copy method copies neuron from other neuron
     * \param r - neuron for copy
     */
    void copy(const neurone & r);

    /*!
     * \brief The mutate_weights method mutates \a mutation_percent of links on
     * delta
     * \param mutation_percent - percent of links which will be mutated
     * \param delta - range for link mutation
     */
    void mutate_weights(ulli mutation_percent, double delta);

    void neuro_love(const neurone & p1, const neurone & p2);

    /*!
     * \brief The optimize method optimize low weight value links
     * (may be I will use it in future versions)
     * \param optimize_param - optimization param
     */
    void optimize(double optimize_param);

    /*!
     * \brief The mutate_struct method change linkink in a neuron
     * (may be I will use it in future versions)
     * \param mutation_percent - percent of changing links
     * \param optimize_param - optimization param
     */
    void mutate_struct(ulli mutation_percent, double optimize_param);

    /*!
     * \brief serialize - saves state of neuron to string
     * \param state - string to saving
     */
    void serialize(std::string &state) const;

    /*!
     * \brief deserialize - loads state of neuron from string
     * \param buffer - string for loading
     * \return count of deserialized links
     */
    ulli deserialize(const std::string &buffer);

};









}// namespace n_engine

#endif // NEURONE_H
