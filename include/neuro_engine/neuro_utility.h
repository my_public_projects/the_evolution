#ifndef NEURO_UTILITY_H
#define NEURO_UTILITY_H
#include <cmath>
#include <random>

using ulli = unsigned long long int;

enum class activation_type : int
{
    sigmoid = 0,
    tanh,
    relu,
    percepto_tanh
};

///
///Realiztion of activation functions of neurones
///

/*!
 * \brief The sigmoid activation function
 * \param x - input
 * \return double
 */
inline double sigmoid(double x) noexcept
{
    return 1.0/(1.0 + std::pow(M_E, -x));
}

/*!
 * \brief Tha activation function
 * \param x - input
 * \return double
 */
inline double tanh(double x) noexcept

// (2 / (1+e^(-2x)) - 1
{

    return  (2 / (1 + std::pow(M_E, -2.0 * x))) - 1;
//    return (1.0 - std::pow(M_E, -2.0 * x)) /
//            (1.0 + std::pow(M_E, -2.0 * x));
}

inline double percepto_tanh(double x)
{
    double temp = tanh(x);
    return temp > 0.5 ? 1 : (temp < -0.5 ? -1 : 0);
}

/*!
 * \brief The relu activation function
 * \param x - input
 * \return double
 */
inline double relu(double x) noexcept
{
    return  x >= 0.0 ? x : 0.0;
}


#endif // NEURO_UTILITY_H
