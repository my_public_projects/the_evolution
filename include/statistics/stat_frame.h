#ifndef STAT_FRAME_H
#define STAT_FRAME_H

#include <map>
#include <memory>
#include <vector>

using data_map = std::map<std::string, double>;

/*!
 * \brief The stat_frame class one frame of statistics data. Contains map of
 * parameters name and they values
 */
class stat_frame
{
    std::shared_ptr<data_map> m_data;
    std::size_t m_time_stamp;


public:

    stat_frame();

    stat_frame(const std::vector<std::pair<std::string, double>> & params);

    /*!
     * \brief The add_param method adds pair of parameter name and value
     * \param param_name
     * \param param_value
     */
    void add_param(const std::string & param_name,
                   double param_value);


    /*!
     * \brief The get_data method gives const access to map of parameters
     * \return shared pointer to constant map of parameters
     */
    std::shared_ptr<const data_map>
    get_data() const;

    /*!
     * \brief The get_tick_stamp method returns tick stamp when been created
     * \return tick stamp
     */
    std::size_t get_tick_stamp() const;

    /*!
     * \brief The serialize saves frame to string
     * \return saved string
     */
    std::string serialize() const;

    /*!
     * \brief The deserialize method loads frame from string
     * \param data
     */
    void deserialize(const std::string & data);

};
#endif // STAT_FRAME_H
