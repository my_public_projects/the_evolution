#ifndef PARAM_VALUE_SET_H
#define PARAM_VALUE_SET_H
#include "stat_frame.h"

/*!
 * \brief The param_value_set class holds vector ouf stat. frames
 * (like a in python's numpy array or pandas's column). Used for holding values
 * of one parameter and do math operations over them
 */
class param_value_set : public std::vector<std::shared_ptr<stat_frame>>
{
    /*!
     * \brief m_name - name of parameter
     */
    std::string m_name;

public:

    param_value_set();

    param_value_set(const std::string & param_name);

    /*!
     * \brief The mean method calculates mean of values
     * \return value of mean
     */
    double mean();


    /*!
     * \brief The name method gives access to name of parameter
     * \return string name
     */
    const std::string & name() const;

    /*!
     * \brief The set_name method sets value of name of perameter
     * \param name - new name
     */
    void set_name(const std::string & name);



};
#endif // PARAM_VALUE_SET_H
