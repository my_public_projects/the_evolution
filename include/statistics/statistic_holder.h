#ifndef STATISTIC_HOLDER_H
#define STATISTIC_HOLDER_H
#include <memory>
#include <map>
#include <vector>
#include <list>
#include <fstream>
#include <assert.h>
#include <chrono>
#include <mutex>
#include <sstream>
#include <iomanip>
#include "defines.h"
#include "logger.h"

#include "stat_frame.h"
#include "stat_writer.h"
#include "param_value_set.h"

/*!
 * \brief The stat_holder class holds and writes statistics data.
 */
class stat_holder
{
    /*!
     * \brief m_primary_list - base list for data holding
     */
    std::shared_ptr<std::list<std::shared_ptr<stat_frame>>> m_primary_list;

    /*!
     * \brief m_secondary_list - temporary list for data holding
     * \details Uses for put data while data from primary list write to file.
     */
    std::shared_ptr<std::list<std::shared_ptr<stat_frame>>> m_secondary_list;

    /*!
     * \brief m_data_frames - used for replacement primary list and secondary
     */
    std::shared_ptr<std::list<std::shared_ptr<stat_frame>>> m_data_frames;

    /*!
     * \brief m_param_sets - map with param set
     */
    std::map<std::string, param_value_set> m_param_sets;
    std::mutex m_param_set_mutex;

    /*!
     * \brief m_max_size - if size of primary list greater this parameter then
     * half part of data will been writed to file
     */
    size_t m_max_size = 1000;

    /*!
     * \brief m_push_counter - is counter of pushed frames
     */
    size_t m_push_counter = 0;

    /*!
     * \brief m_data_frames_mutex - mutex for thread safe pushing frames and
     * swapping of lists of data
     */
    std::mutex m_data_frames_mutex;

    /*!
     * \brief m_writer - instance of stat_writer class for witing data to file
     */
    stat_writer m_writer;

    /*!
     * \brief m_calculator_thread - thread for async writing to file
     */
    std::thread m_calculator_thread;

    /*!
     * \brief m_is_not_join - flag for stoping worker
     */
    bool m_is_not_join = true;

    /*!
     * \brief The capture_primary method captures primary list
     * (m_data_frames = m_secondary_list)
     */
    void capture_primary();


    /*!
     * \brief The parse_frame method parses frame to param sets
     * \param frame shared pointer to frame for parsing
     */
    void parse_frame(const std::shared_ptr<stat_frame> & frame);


    /*!
     * \brief The reparse_sets method clears m_param_sets and reparses frames
     * from primary list
     */
    void reparse_sets();

    /*!
     * \brief The push_data method pushs data to m_data_frames
     * \param frame
     */
    void push_data(const std::shared_ptr<stat_frame> & frame);

    /*!
     * \brief The attach_secondary method appends secondary list to primary
     */
    void attach_secondary();

    /*!
     * \brief The relize_primary method relizes primary list
     * (m_data_frames = m_primary_list)
     */
    void relize_primary();

    /*!
     * \brief The worker method is worker runing in m_calculator_thread.
     * Checks size of primary list if it's size greater m_max_size then half
     * part of data will been writed to file
     */
    void worker();


    /*!
     * \brief The write method writes data to file
     * \param count - count of frames which will be left after writing in list
     */
    void write(size_t count);
public:
    stat_holder(const std::string & out_file_name);

    ~stat_holder();

    /*!
     * \brief The push method public interface for pushing data to holder
     * \param frame - new frame for holding
     */
    void push(const stat_frame & frame);

    /*!
     * \brief The mean method calculates mean of parametr's value from store
     * \param param_name - parameter's name
     * \return value of mean of parameter's values in store
     */
    double mean(const std::string & param_name);


};
#endif//class stat_holder`
