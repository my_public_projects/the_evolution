#ifndef STAT_WRITER_H
#define STAT_WRITER_H
#include <fstream>
#include <list>
#include <memory>
#include "stat_frame.h"
/*!
 * \brief The stat_writer class writes statistic's data to file
 */
class stat_writer
{
    std::fstream m_fstream;
public:
    stat_writer(const std::string & file_name);

    ~stat_writer();

    /*!
     * \brief The write method writes data to file
     * \param data - list of statistic frames
     * \param count - count of frames which will be left after writing in list
     */
    void write(std::list<std::shared_ptr<stat_frame>> & data, size_t count);

};
#endif // STAT_WRITER_H
