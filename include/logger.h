#ifndef LOGGER_H
#define LOGGER_H
#include <string>
#include <memory>
#include <queue>
#include <mutex>
#include <thread>
#include <fstream>
#include <chrono>
#include <charconv>
#include <assert.h>

#define l_str std::string()

#define out_head (l_str + type_name(*this) + "::" + __FUNCTION__ + "==> ")

#ifndef NDEBUG
#define log_deb_inf(mess) logger::info(l_str+"\033[34mDEBUG_INFO: "+mess+"\033[0m")
#define log_deb_warn(mess) logger::warn(l_str+"\033[35mDEBUG_WARN: "+mess+"\033[0m")
#define log_deb_err(mess) logger::error(l_str+"\033[36mDEBUG_ERROR: "+mess+"\033[0m")
#else
#define log_deb_inf(mess)
#define log_deb_warn(mess)
#define log_deb_err(mess)
#endif


//-------------------------From stackoverflow.com
/*!
 * \brief
 */
std::string demangle(const char* name);


// Get readable type name
template <class T>
std::string type_name(const T& t) {

    return demangle(typeid(t).name());
}

//-----------------------------------------------

template <class T>
/*!
 * \brief The to_std_str function for convert numbers to std::string
 * representaion
 * \param t - some number of one of numeric types
 * \return string representation of number
 */
std::string to_std_str(const T & t)
{
    std::string str;
    str.resize(21, 0);
    std::to_chars_result to_ch =
            std::to_chars(str.data(), str.data()+str.size(), t) ;

    assert(to_ch.ec == std::errc());
    //NRVO hope
    return str;
}


/*!
 * \brief The message_queue class simple realization of threadsafe queue of
 * logger messges
 */
class message_queue {

public:
    //using message = std::shared_ptr<std::string>;
    /*!
     * \brief The message struct contains string data of message, timestamp when
     *  message has been created and id of thread that message was been created
     */
    struct message
    {
        std::time_t m_stamp;
        std::string m_data;
        std::thread::id m_thread_id;

        message(const std::string & data)
            : m_stamp(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())),
              m_data(data),
              m_thread_id(std::this_thread::get_id())
        {

        }


        message(std::string && data)
            : m_stamp(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())),
              m_data(std::move(data)),
              m_thread_id(std::this_thread::get_id())
        {

        }

    };
private:
    std::mutex m_mutex;

    std::queue<std::shared_ptr<message>> m_queue;


public:


    message_queue();

    /*!
     * \brief The push method puts message in the queue
     * \param _message - new message
     */
    void push(const std::shared_ptr<message> &_message);

    /*!
     * \brief The pop method pops message from front of queue
     * \return  shared pointer to message
     */
    std::shared_ptr<message> pop();

    /*!
     * \brief The size method returns count of messages in queue
     * \return length of queue
     */
    size_t size();

}; // class comands_queue


class logger
{

    /*!
     * \brief m_messages - thread safe queue of messges
     */
    message_queue m_messages;

    /*!
     * \brief m_run - flag for stop logger thread
     */
    bool m_run = true;

    /*!
     * \brief m_thread - logger thread
     */
    std::thread m_thread;

    /*!
     * \brief m_log_filename - file name for logs write
     */
    std::string m_log_filename;

    /*!
     * \brief m_fstream - file stream for loging writing to file
     */
    std::fstream m_fstream;

    //Rulle of five
    logger();
    logger(const logger&) = delete;
    logger(const logger&&) = delete;

    logger & operator = (const logger&) = delete ;
    logger & operator = (const logger&&) = delete ;

    ~logger();

    /*!
     * \brief The private method print - composes message's timestamp and
     * message's text to string for write
     * \param mess - shared pointer to message
     */
    void print(const std::shared_ptr<message_queue::message> & mess);

    /*!
     * \brief The write method - writes composed string to std::cout and
     * m_fstream
     * \param data - string for write
     */
    void write(const std::string & data);

    /*!
     * \brief The process method is a worker method. It method running in the
     * logger thread and processes messages in message queue
     */
    void process();

    /*!
     * \brief The instance method is gives access to logger instance
     * \return reference to instance of logger class
     */
    static logger & instance();

    /*!
     * \brief The init method initializes logger with file name for data writing
     * \param log_file_name - string name of file
     */
    void init_pvt(const std::string & log_file_name);
public:


    /*!
     * \brief The init initialze logger with file name for logs write
     * \param log_file_name
     */
    static void init(const std::string & log_file_name);

    /*!
     * \brief The info method is logs a information message
     * \param mess - string information message
     * \param new_line - bueee
     */
    static void info(const std::string & mess, bool new_line = true);

    /*!
     * \brief The info method is logs a information message(move semantics)
     * \param mess - string information message (rvalue)
     * \param new_line - I will not say!
     */
    static void info(std::string && mess, bool new_line = true);

    /*!
     * \brief The warn method is logs a warning message
     * \param mess - string warning message
     * \param new_line - NEVER!
     */
    static void warn(const std::string & mess, bool new_line = true);

    /*!
     * \brief The warn method is logs a warning message(move semantics)
     * \param mess - string warning message(rvalue)
     * \param new_line - I said NEVER!
     */
    static void warn(const std::string && mess, bool new_line = true);

    /*!
     * \brief The error method is logs a error message
     * \param mess - string error message
     * \param new_line - Enough!
     */
    static void error(const std::string & mess, bool new_line = true);

    /*!
     * \brief The error method is logs a error message(move semantics)
     * \param mess - string error message(move semantics)
     * \param new_line - NOOO! I will cry!
     */
    static void error(const std::string && mess, bool new_line = true);

    /*!
     * \brief The time_to_string method (in fact it function) converts
     * std::time_t to string format of date
     * \param t - time point
     * \return string formated date
     */
    static
    std::string time_to_string(std::time_t t);
};




#endif // LOGGER_H
