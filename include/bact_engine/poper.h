#ifndef POPER_H
#define POPER_H
#include "object.h"

namespace b_engine {
class particle;
class poper : public object
{

    std::vector<std::shared_ptr<object>> m_particles;

    std::shared_ptr<particle> make(float r);

    void generate(float radius);


public:

    poper(float radius, float x, float y);
    virtual ~poper() override;

    // object interface
public:
    void draw(sf::RenderWindow &window) override;
    bool check_collision(const object_ptr obj) override;

protected:
    void on_update(const std::vector<object_ptr> &objects) override;
    void on_collision(const object_ptr obj) override;
    void on_destroy() override;
};
}
#endif // POPER_H
