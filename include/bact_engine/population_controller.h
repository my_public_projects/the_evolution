﻿#ifndef POPULATION_CONTROLLER_H
#define POPULATION_CONTROLLER_H
#include "neuro_bact.h"
#include "neuro_engine/ann.h"
#include "statistics/statistic_holder.h"
#include "terminal.h"
#include "multithreading/queue.h"


namespace b_engine {

using n_bact_ptr = std::shared_ptr<neuro_bact>;
using brain_ptr = std::shared_ptr<n_engine::ann>;



struct leader
{
    n_bact_ptr creature;
    lli score;
};

struct population
{
    lli m_score = 0l;
    std::string m_serial;
};

/*!
 * \brief The popul_param struct represents parameters of population and
 *  ANN's params
 */

struct popul_param
{
    /*!
     * \brief The brain_struct represents structure of ann by layers
     */
    std::vector<n_engine::layer_param> brain_struct;

    /*!
     * \brief The reccurent if true then ANNs will be reccurent links
     */
    bool reccurent = true;

    /*!
     * \brief The new_gen_mutation_weights_percent is percent of neurons links
     * in new population generation time, which will be mutated
     */
    ull new_gen_mutation_weights_percent = 0;

    /*!
     * \brief The new_gen_mutation_weights_delta is max correcting range of
     * link in mutating time
     */
    double new_gen_mutation_weights_delta = 0;

    /*!
     * \brief The new_gen_mutation_struct_percent is percent of network
     * structure mutation in next generation. (To the future)
     */
    ull new_gen_mutation_struct_percent = 0;

    /*!
     * \brief The new_gen_mutation_struct_opt_param is links optimization param.
     * If an link's weight absolute value lower that parm then this link will
     * be removed. (To the future)
     */
    double new_gen_mutation_struct_opt_param = 0;

    /*!
     * \brief The children_mutation_weights_percent is percent of neurons links
     * in chldren generation time, which will be mutated. (To the future)
     */
    ull children_mutation_weights_percent = 0;

    /*!
     * \brief The children_mutation_weights_delta is max correcting range of
     * link in children's mutating time. (To the future)
     */
    double children_mutation_weights_delta = 0;

    /*!
     * \brief The children_mutation_struct_percent is links optimization param.
     * If an link's weight absolute value lower that parm then this link will
     * be removed. (To the future)
     */
    ull children_mutation_struct_percent = 0 ;

    /*!
     * \brief The children_mutation_struct_opt_param is links optimization param.
     * If an link's weight absolute value lower that parm then this link will
     * be removed. (To the future)
     */
    double children_mutation_struct_opt_param = 0;

    /*!
     * \brief The population_size is count of Individuals in population
     */
    ull population_size = 30;

    /*!
     * \brief The next_target param is target of mean of score which population
     * must  achieved for optimize mutation parameter
     */
    lli next_target = 500;

    /*!
     * \brief The target_multipler param is multiplier of next target when
     * target was achivied
     */
    double target_multiplier = 1.2;

    /*!
     * \brief The log method prints parameters of class instance to log
     */
    void log();


    /*!
     * \brief The serialize method saves all params to string.
     * \param state - string for saving
     */
    void serialize(std::string & state);

    /*!
     * \brief The deserialize method load parametrs values from string
     * \param state - data string for restore values
     */
    void deserialize(const std::string & state);


};

class popul_controller : public object
{

    /*!
     * \brief The m_population_bodys is vector of pointers to neuro_bact
     * instance
     */
    std::vector<n_bact_ptr> m_alive_bacts;

    /*!
     * \brief The m_valhalla is vector of death bacts with scores
     * (life after death / all anns after death goes to m_valhalla)
     */
    std::vector<leader> m_valhalla;

    std::vector<std::shared_ptr<population>> m_helheim;

    /*!
     * \brief The m_brain_param is holds params of population.
     */
    popul_param m_brain_param;

    /*!
     * \brief The m_generation_counter - counts populations
     */
    ull m_generation_counter = 0;

    /*!
     * \brief The m_last_pop_tick_count - holds tick stamp(like a time stam but
     * tick stamp) of death of previous population
     */
    ull m_last_pop_tick_count = 0;

    /*!
     * \brief The m_last_life_time - tick count of life time previous population
     */
    ull m_last_life_time = 0;

    /*!
     * \brief The m_stat_holder - instance of stat_holder for ststistics
     * collecting
     */
    stat_holder m_stat_holder;

    /*!
     * \brief m_commands - thread safe queue for handling terminal commands
     */
    std::shared_ptr<multithreading::queue<comm_message::ptr>> m_commands;


    /*!
     * \brief The create_brain method creates new instance of ann
     * \return shared pointer to created ANN
     */
    std::shared_ptr<n_engine::ann> create_brain();

    /*!
     * \brief The on_bact_death method handles bact's death and push it's brain
     * in to m_leaders vector
     * \param bact - pointer to the bact
     */
    void send_to_valhala(const n_bact_ptr & bact);

    /*!
     * \brief The sort_leaders method sorts m_leaders vector by ascending of score
     */
    void sort_leaders();

    /*!
     * \brief The clear method clears vectors m_population_bodys m_free_bodys
     * m_population_brains.clear() m_free_brains.clear
     */
    void clear();

    /*!
     * \brief The auto_save method saves automaticly populations every 1000
     * populations(that param is hard coded in cpp file it's bad)
     */
    void auto_save();

    /*!
     * \brief The get_life_time method calculates life time in ticks count
     * \return
     */
    ull get_life_time();

    /*!
     * \brief The correct_mutauion_delta_by_life_time method corrects mutation
     * delta if \a mean_life_time overgrows target
     * \param mean_life_time - mean of life time which calculates by values
     * holds in stat_holder
     */
    void correct_mutauion_delta_by_life_time(size_t mean_life_time);

    /*!
     * \brief The correct_mutation_delta_by_mean_score method corrects mutation
     * delta by mean value of scores in population
     * \param mean_score - mean score of in population
     */
    void correct_mutation_delta_by_mean_score(lli mean_score);


    /*!
     * \brief The get_mean_score method claculate mean of score in population
     * \return mean of score in population
     */
    lli get_mean_score();

    /*!
     * \brief The make_copy_and_mutation method copy weights from parent to
     * child and calls child's mutation
     * \param parent
     * \param child
     * \param mutation_percent - percent of weights for mutation
     * \param mutation_delta - range for mutation
     */
    void make_copy_and_mutation(const brain_ptr & parent,
                                const brain_ptr & child,
                                std::size_t mutation_percent,
                                double mutation_delta);

    void make_fuck_and_mutation(const brain_ptr & parent_one,
                                const brain_ptr & parent_two,
                                const brain_ptr & child,
                                std::size_t mutation_percent,
                                double mutation_delta);

    /*!
     * \brief The to_helheim method serialize population and sends it's data
     * for storing to m_helheim
     * \param mean_score - score of popualtion
     */
    void to_helheim(size_t mean_score);

    /*!
     * \brief The from_helheim method loads best population from m_helheim and
     * clears it
     */
    void from_helheim();

    /*!
     * \brief The respawn_bact method respawns bact from valhala
     * \param bact - shared pointer to bact for respawning
     * \return true if free place for bact was found
     */
    bool respawn_bact(const n_bact_ptr & bact);

    /*!
     * \brief The reborn_warrior method creates \a warrior's child with
     * mutations(it takes a body from m_valhala and rewrites it's brains)
     * \param warrior - parent
     * \return true if success
     */
    bool reborn_warrior(const n_bact_ptr & warrior);

    /*!
     * \brief The warriors_love method borns chold from \a warrior1 and
     * \a warrior2
     * \param warrior1 - parent number one
     * \param warrior2 - parent number two
     * \return - true if success
     */
    bool warriors_love(const n_bact_ptr & warrior1,
            const n_bact_ptr & warrior2);

    bool make_child(const n_bact_ptr & mommy);

    void make_popualtion();

    /*!
     * \brief The make_crossover method generates next
     * population by crossingover(crossover) algorithm
     * \return true if succses generated
     */
    void make_crossingover();

    /*!
     * \brief The generate_next_population method generates next population
     * \return true if succses generated
     */
    bool generate_next_population();

    /*!
     * \brief The save_command method handling the save population command from
     * terminal
     * \param comm - string command
     */
    void save_command(const std::string & comm);

    /*!
     * \brief The load_command method handling the load population command from
     * treminal
     * \param comm - string command
     */
    void load_command(const std::string & comm);

    //---------HANDLING TERMINAL COMMANDS----------
    enum class commands : int
    {
        save_population,
        load_population
    };
    /*!
     * \brief The make_commands method adds commands to terminal
     */
    void make_commands();

    /*!
     * \brief The remove_commands method rmoves commands from trminal
     */
    void remove_commands();

    /*!
     * \brief The process_commands method handles m_commands
     */
    void process_commands();

    /*!
     * \brief The current_life_time method
     * \return current life time of current population.
     */
    ull current_life_time();

public:
    popul_controller();
    popul_controller(const popul_param & ann_param);

    virtual ~popul_controller();

    /*!
     * \brief The generate method generates zero population
     */
    virtual void generate();

    /*!
     * \brief The next_gen method generates next generation with mutations
     * after death of previous population
     * \return true if success false if fail
     */
    virtual bool next_gen();

    /*!
     * \brief The update method checks population is alive if population is dead
     * then calls next_gen method
     */
    virtual void update();

    /*!
     * \brief The brain_param method returns copy of m_brain_param
     * \return popul_param instance
     */
    popul_param brain_param() const;

    /*!
     * \brief The set_brain_param method overwrites m_brain_param struct
     * \param brain_param - new params
     */
    void set_brain_param(const popul_param &brain_param);

    /*!
     * \brief The serialize_population method serileze current population to
     * string data
     * \param population - reference to string data for serialization
     */
    void serialize_population(std::string &population);

    /*!
     * \brief The deserialize_population method
     * \param population
     */
    void deserialize_population(const std::string &population);

    /*!
     * \brief The pop_save method saves current population to file
     * \param file_name - name of file for saving
     */
    void pop_save(const std::string & file_name);

    /*!
     * \brief The pop_load method loads population from file.
     * The current population will be overwrited.
     * \param file_name - file name for load
     */
    void pop_load(const std::string & file_name);

protected:

    /*!
     * \brief The find_palce method finds free space in scene for neuro_bact
     * instance
     * \param _bact - bact for placing
     * \param objs - scene objects vector
     * \return true if free space has found false otherwise
     */
    bool find_palce(
            const n_bact_ptr & _bact,
            const std::vector<object_ptr> & objs);

    /*!
     * \brief The make_bact method creates new neuro_bact instance in free
     * space location
     * \return shared pointer to new bact if free space has found,
     * nullptr instantiated shared pointer otherwise;
     */
    virtual n_bact_ptr make_bact();

    // object interface
public:
    void draw(sf::RenderWindow &);
    bool check_collision(const object_ptr){return true;}

protected:
    void on_update(const std::vector<object_ptr> &){update();}
    void on_collision(const object_ptr){};
    void on_destroy(){};
};
} // namespace b_engine
#endif // POPULATION_CONTROLLER_H
