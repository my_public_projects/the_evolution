#ifndef OBJECT_H
#define OBJECT_H
#include "defines.h"
#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>
namespace b_engine {
class object;
using object_ptr = std::shared_ptr<object>;
using const_object_ptr = std::shared_ptr<const object>;


sf::Color get_rand_color();

void mutate_color(sf::Color & color, double delta);


/*!
 * \brief The object class is base abstract class
 */
class object
{
    friend class engine;
    object_type m_type;
    float m_radius = 2.0f;
    float m_x = 0.0f;
    float m_y = 0.0f;
    float m_mass = 1.0f;
    bool m_need_remove = false;
    bool m_is_destroy = false;
    sf::Color m_color = sf::Color(255, 255, 255);



public:


    object(object_type type);
    virtual ~object();


    /*!
     * \brief The y method
     * \return Y axis coordinate value of object
     */
    float y() const;

    /*!
     * \brief The x method
     * \return X axis coordinate value of object
     */
    float x() const;

    /*!
     * \brief The set_x method sets X axis coordinates.
     * \param x - new value
     */
    void set_x(float x);

    /*!
     * \brief The set_y method sets Y axis coordinates.
     * \param y - new value
     */
    void set_y(float y);

    /*!
     * \brief The set_pos method sets new position of object in scene
     * \param x - X axis coordinate
     * \param y - Y axis coordinate
     */
    void set_pos(float x, float y);

    /*!
     * \brief The update method base update method calls on_update from heirs.
     * \param objects - objects in scene
     */
    void update(const std::vector<object_ptr> &objects);

    /*!
     * \brief The type method needs for avoid RTTI using
     * \return enumeration element of object_type <defines.h>.
     */
    object_type type() const;


    /*!
     * \brief The radius method. Radius calculating by
     * m_radius = std::sqrt(m_mass/g_density/g_pi);
     * \return radius of object
     */
    float radius() const;

    /*!
     * \brief The mass method
     * \return value of object's mass
     */
    float mass() const;

    /*!
     * \brief The set_mass sets value of object mass
     * \param mass - new value
     */
    void set_mass(float mass);

    /*!
     * \brief The delta_mass method adds delta value to mass
     * \param delta - positive/negative delta
     */
    void delta_mass(float delta);

    /*!
     * \brief The destroy method marks object for removing from scene by engine
     */
    void destroy();

    /*!
     * \brief The respawn method make object alive again.
     */
    virtual void respawn();

    /*!
     * \brief The need_remove method says to engine, but object need will remove
     * \return true if destroy method was been called
     */
    bool need_remove()
    {
        return m_need_remove;
    }

    /*!
     * \brief The on_remove method marks object what it was removed
     */
    void on_remove()
    {
        m_is_destroy = true;
    }

    /*!
     * \brief The is_destroy method
     * \return true if object was been removed from engine
     */
    bool is_destroy() const;

    /*!
     * \brief The abstract method for drawing self
     * \param window - window for rendering
     */
    virtual void draw(sf::RenderWindow & window) = 0;

    /*!
     * \brief The check_collision method is abstract method for
     * collision detection
     * \param obj - object for collision check
     * \return must return true if collision is occurred
     */
    virtual bool check_collision(const object_ptr  obj) = 0;


    /*!
     * \brief The think method for heirs for make solution
     * \details This method must be threadsafe about other objects
     */
    virtual void think(const std::vector<object_ptr> &) {}

    /*!
     * \brief The set_color method sets color for object drawing
     * \param color - sf::Color
     */
    void set_color(const sf::Color & color);

    /*!
     * \brief The get_color method gives access to drawing color.
     * \return Reference to const Color
     */
    const sf::Color & get_color() const;



protected:


    /*!
     * \brief The on_update method is abstract method for updating states of heir objects
     * \param objects - all scene objects
     */
    virtual void on_update(const std::vector<object_ptr> & objects) = 0;

    /*!
     * \brief The on_collision method heirs handles collision
     * \param obj - Object with which collision was been occured
     */
    virtual void on_collision(const object_ptr obj) = 0;

    /*!
     * \brief The on_destroy method for heirs handling destroying
     */
    virtual void on_destroy() = 0;

};



}//namespace b_engine
#endif // BACT_H




