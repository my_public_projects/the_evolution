#ifndef PARTICLES_H
#define PARTICLES_H
#include "object.h"
namespace b_engine
{
/*!
 * \brief The particle class represents FX of particles
 */
class particle : public object
{
    int m_life = g_paricle_live;
    sf::Vector2f m_speed;
    sf::CircleShape m_circle;
    sf::Color m_color;

    void init();
public:
    particle(float v_x, float v_y);

    particle(float v_x, float v_y, const sf::Color & color);

    virtual ~particle() override;

    void draw(sf::RenderWindow &window) override;

protected:
    void on_update(const std::vector<object_ptr> &objects) override;
    bool check_collision(const object_ptr obj) override;
    void on_collision(const object_ptr obj) override;
    void on_destroy() override;
};
}//namespace b_engine
#endif // PARTICLES_H
