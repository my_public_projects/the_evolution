#ifndef FOOD_GENERATOR_H
#define FOOD_GENERATOR_H
#include <memory>
#include <vector>
#include "defines.h"
#include "leaf.h"

namespace b_engine {
/*!
 * \brief The food_generator class controls count of leaf instances
 */
class food_generator : public object
{
    using leaf_ptr = std::shared_ptr<leaf>;
    std::vector<leaf_ptr> m_food;

    /*!
     * \brief The find_place method finds empty space in scene for respawn leaf
     * \param _food - pointer to respawning leaf
     * \param objs - vector of scene's objects
     * \return true if space is founded false otherwise
     */
    bool find_place(const leaf_ptr & _food, const std::vector<object_ptr> & objs);


    /*!
     * \brief The respawn mathod respawns leaf with @a index
     * \param index - index of leaf in m_food vector
     * \return false if find_place method returns false otherwise true
     */
    bool respawn(ull index);






public:

    food_generator();

    /*!
     * \brief The generate method generates <g_leaf_number> instanses of
     * leaf class
     */
    void generate();

    /*!
     * \brief The update method keeps count of alive leafs equal <g_leaf_number>
     */
    void update();


    // object interface
public:
    void draw(sf::RenderWindow &){}
    bool check_collision(const object_ptr)
    {return false;}

protected:
    void on_update(const std::vector<object_ptr> &){update();}
    void on_collision(const object_ptr){}
    void on_destroy(){}
};
}// namespace b_engine
#endif // FOOD_GENERATOR_H
