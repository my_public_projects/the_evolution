#include "object.h"
#include "bact.h"
#include "render/render.h"
#include "terminal.h"
#include "multithreading/thread_pool.h"

#include <algorithm>
#include <mutex>
#include <SFML/Graphics.hpp>


namespace b_engine {
/*!
 * \brief The engine class holds and process all objects in scene.
 * It is the Meyers' Singleton. Yes, i know...
 */
class engine
{
    render m_render;

    bool m_is_run = false;
    bool m_paused = false;
    bool m_multi_threading = false;
    std::mutex m_jobs_mutexl;
    std::condition_variable m_cond;

    std::vector<size_t> m_f_indexes;
    std::vector<object_ptr> m_objects;

    std::mutex m_obj_mutex;

    ull m_ticks_couner = 0;

    multithreading::thread_pool m_thread_pool;


    //------------Terminal commands handling-----------
    /*!
     * \brief m_commands - thread safe queue for handling terminal commands
     */
    std::shared_ptr<multithreading::queue<comm_message::ptr>> m_commands;
    enum class commands :int
    {
        window_show,
        window_hide,
        exit,
        spawn_player,
        calc_tps,
        pause,
        multithreading
    };

    /*!
     * \brief The spawn_player method creates bact under control from keyboard.
     * This method can be called from terminal command
     */
    void spawn_player();

    /*!
     * \brief The calc_tps method calculates ticks per second. Calls from
     * terminal command
     */
    void calc_tps();

    /*!
     * \brief The pause method pauses and unpauses objects updating
     */
    void pause();

    /*!
     * \brief The show_window method creates the render window
     */
    inline void show_window()
    {
        m_render.show();
    }

    /*!
     * \brief The hide_window method close the render window
     */
    inline void hide_window()
    {
        m_render.close();
    }

    /*!
     * \brief The multithreading method turns on multithreading
     */
    void multithreading();

    //------------Functional-----------
    /*!
     * \brief The process_commands method processes terminal commands queue
     */
    void process_commands();

    engine();

    engine(const engine&) = delete;
    engine(engine &&) = delete ;
    engine & operator = (const engine&) = delete ;
    engine & operator = (engine &&) = delete ;

    ~engine();


    /*!
     * \brief The solve_collisions method checks collisions and if it ocured
     * then calls object::on_collision method
     * \param a - object for check
     * \param b - object for check
     */
    void solve_collisions(object_ptr a, object_ptr b);

    /*!
     * \brief The thinker method is a "job" for threadpool. It make a batch of
     * objects think
     * \param from - batch begin
     * \param n - bach length
     * \param flag [out] - pointer to flag for increment jobs counter.
     */
    void thinker(std::vector<std::shared_ptr<object> > *t_objs,
            size_t from,
            size_t n,
            size_t *flag);

    /*!
     * \brief The make_objects_think method divide objects to batchs and submit
     *  jobs to threadpool
     */
    void make_objects_think(
            std::vector<std::shared_ptr<object> > &think_objs,
            size_t per_job,
            size_t job_count);

    /*!
     * \brief The update_objects method updates all objects in scene
     */
    void update_objects();

    /*!
     * \brief The update_objects multithreaded method make objects think
     * multithreaded and updates objects in main thread
     */
    void update_objects_multithread();

    /*!
     * \brief The update method is main method. It updates state of engine,
     * food_generator, population_controller, renderer.
     */
    void update();


    void remove_object(size_t index);

public:

    static engine & instance();

    /*!
     * \brief The add_object method adds new object to scheme
     * \param obj - shared pointer to new object;
     */
    void add_object(const object_ptr & obj);

    /*!
     * \brief The stop method stops the engine loop.
     */
    void stop()
    {
        m_paused = false;
        m_is_run = false;
    }

    /*!
     * \brief The run method starts the engine's endless loop in current thread
     */
    void run();
    void run_thread();


    /*!
     * \brief The ticks_couner method
     * \return count of ticks from begin of engine's loop
     */
    inline  ull ticks_couner() const
    {
        return m_ticks_couner;
    }

    /*!
     * \brief The objects method
     * \return vector of scene's objects
     */
    inline const std::vector<object_ptr> &objects() const
    {
        return m_objects;
    }

    /*!
     * \brief The is_drawing method
     * \return true if render window is showed
     */
    inline bool is_drawing()
    {
        return m_render.showed();
    }

};





}//namespace b_engine


