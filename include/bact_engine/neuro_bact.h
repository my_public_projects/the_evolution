#ifndef NEURO_BACT_H
#define NEURO_BACT_H
#include "bact.h"

namespace n_engine
{
/*!
 * Forward declaration for Neural Network class
 */
class ann;
}

namespace b_engine
{

class neuro_bact :public bact
{
    std::vector<double> m_input;
    std::vector<double> m_output;
    float m_fx = 0.;
    float m_fy = 0.;

    size_t m_age = 0;

public:
    /*!
     * \brief The death_reason enum
     */
    enum class death_reason : int
    {
        none = 0,
        eatten , //bact was eaten
        hungry, // hungry death
        so_fat, // death from overeating
        leave_world, // was out go of world border
        so_old
    };

    neuro_bact();

    virtual ~neuro_bact() override;

    /*!
     * \brief The brain method gives access to Neural Network instance.
     * \return shared pointer to ANN
     */
    //TODO Избавиться от этого, мне не нравится это. UPD: теперь это норма)
    std::shared_ptr<n_engine::ann> brain() const;

    /*!
     * \brief The set_brain method sets the brains to the bact
     * \param brain shared pointer to ANN
     */
    void set_brain(const std::shared_ptr<n_engine::ann> &brain);

    /*!
     * \brief The score method returns count of score points
     * \return count of score points
     */
    lli score() const;

    /*!
     * \brief The set_score method sets score externaly
     * \param score value of score counts
     */
    void set_score(lli score);


    void delta_score(lli delta);

    /*!
     * \brief The brain_index method
     * \return retuns index of bact in population controller
     */
    ull population_index() const;

    /*!
     * \brief The set_brain_index method sets index of bact in
     * population controller. This method call in population controller.
     * \param pop_index index value
     */
    void set_population_index(const ull & pop_index);

    /*!
     * \brief The get_death_reason method gets access to reason of death of bact
     * \return one value of enum class death_reason
     */
    inline death_reason get_death_reason() const
    {
        return m_d_reason;
    }


    /*!
     * \brief The think method calculates thrust vector.
     * In this method signals from sensors sets to ANN input and
     * ANN makes a solution, output value sets to thrust vector
     * \param objects
     */
    virtual void think(const std::vector<object_ptr> &objects) override;

    void respawn() override;

    inline bool wants_division()
    {
        return m_age > g_age_of_adult &&
                m_output[2] > 0.5 &&
                energy() > g_bact_baby_energy;
    }

protected:



    /*!
     * \brief The draw method draws the red circle with bact's position
     * and with bact's radius
     * \param window
     */
    virtual void on_draw(sf::RenderWindow &window) override;
    virtual void on_destroy() override;

    //std::shared_ptr<popul_controller> m_contoller;

    std::shared_ptr<n_engine::ann> m_brain = nullptr;
    ull m_population_index = -1;
    long long int m_score = 0;

    death_reason m_d_reason = death_reason::none;

    // object interface
protected:
    /*!
     * \brief The on_collision process collision with other object in scene
     * \param obj - collision occured object
     */
    void on_collision(const object_ptr obj) override;

    /*!
     * \brief The on_update method updates state of bact.
     * \param objects
     */
    virtual void on_update(const std::vector<object_ptr> &objects) override;

};

}//namespace b_engine
#endif // NEURO_BACT_H
