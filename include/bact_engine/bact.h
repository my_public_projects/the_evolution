#ifndef BACT_H
#define BACT_H
#include "object.h"
namespace b_engine
{
/*!
 * \brief The bact class is abstract class for bacteries.
 */
class bact : public object
{
    float m_force_x = 0.f;
    float m_force_y = 0.f;

    float m_accel_x = 0.f;
    float m_accel_y = 0.f;

    float m_speed_x = 0.f;
    float m_speed_y = 0.f;

    float m_energy = 0.f;

    /*!
     * \brief The calc_movement method calculates delta of position from
     * force vector and adds it to position vector
     *  (f = m*a; a = f/m; v+=a, pos+=v)
     */
    void calc_movement();

    std::vector<object_ptr> m_fart; // particles of thrust
    std::vector<ull> m_free; // free indexses of m_fart vector;

    /*!
     * \brief add_farticle - adds a particle representing the thrust vector.
     * \param f - instanse of particle class
     */
    void add_farticle(object_ptr f);

    /*!
     * \brief the fart_update method for all elements of the
     *  m_fart vector calls the update() method
     */
    void fart_update();

public:
    bact();
    virtual ~bact() override;


    // object interface
    /*!
     * \brief The draw method draws object in render window
     * \param window - reffernce to SFML::RenderWindw
     */
    void draw(sf::RenderWindow &window) override;

    /*!
     * \brief The check_collision method check collisons with other objects
     * from scene. May be overrided.
     * \param obj - a pointer to a object for check
     * \return true if collision is found otherwise false
     */
    virtual bool check_collision(const object_ptr obj)override;

    /*!
     * \brief The speed_x method returns the component X of speed vector
     * \return the component X of speed vector
     */
    inline float speed_x() const noexcept
    {
        return m_speed_x;
    }

    /*!
     * \brief The speed_y method returns the component Y of speed vector
     * \return
     */
    inline float speed_y() const noexcept
    {
        return m_speed_y;
    }

    /*!
     * \brief The set_speed method sets vector of speed
     * \param x - abscissa
     * \param y - ordinate
     */
    inline void set_speed(float x, float y)
    {
        m_speed_x  =x;
        m_speed_y = y;
    }

    /*!
      * \brief The accel_x method retutn acceleration by X
      * \return abscissa componet of acceleration vector
      */
     float accel_x() const;

     /*!
      * \brief The accel_н method retutn acceleration by Y
      * \return ordinate componet of acceleration vector
      */
     float accel_y() const;

     float energy() const
     {
         return m_energy;
     }

     void set_energy(float e)
     {
         m_energy = e;
     }

     void delta_energy(float delta)
     {
         auto temp = m_energy + delta;
         m_energy = temp > 0 ? temp : g_EPS;
     }

     /*!
      * \brief The think method calculates vector of thrust.
      * The bact makes a solution in this method.
      */
     virtual void think(const std::vector<object_ptr> &) override;

protected:

    /*!
     * \brief The on_update method calls in update method.
     * \param objects - all scene objects
     */
    virtual void on_update(const std::vector<object_ptr> &) override;

    /*!
     * \brief The on_collision method will be called if check_collision
     * returns true
     * \param obj - Object with which collision was been occured
     */
    virtual void on_collision(const object_ptr obj) override;

    /*!
     * \brief The on_destroy method will be called before object will be
     * destroyed
     */
    virtual void on_destroy() override;


    virtual sf::Vector2f resolve_collision(const object_ptr & obj);



    /*!
     * \brief The on_draw method allow to heirs of this class draw them selfs
     * \param window
     */
    virtual void on_draw(sf::RenderWindow &window);

    /*!
     * \brief The set_force method sets the thrust vector externaly
     * \param fx - abscissa
     * \param fy - Ordinate
     */
    void set_force(float fx, float fy)
    {
        m_force_x = fx;
        m_force_y = fy;
    }

    //I win!

};
}
#endif // BACT_H
