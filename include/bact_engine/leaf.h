#ifndef LEAF_H
#define LEAF_H
#include "object.h"
namespace b_engine {
/*!
 * \brief The leaf class representates a food for bacteries
 */
class leaf : public object
{
public:
    /*!
     * \brief leaf - constructs instance with random mass and random pos
     */
    leaf();
    virtual ~leaf() override;

    ///TODO Кажется от этого можно избавиться.
    ///Метод исполззуется только для поисика свободного места
    bool is_collide(const object_ptr & obj) const
    {
        float r = g_view_dist;
        r *= r;
        auto  temp = (sf::Vector2f(x(), y()) - sf::Vector2f(obj->x(), obj->y()));
        float sqr_dist = (temp.x * temp.x + temp.y * temp.y);
        return r > sqr_dist;
    }

    // object interface
    /*!
     * \brief The draw method draws the green circle with leaf's position
     * and with leaf's radius
     * \param window
     */
    void draw(sf::RenderWindow &window) override;


    /*!
     * \brief The check_collision method overrides abstract method of the
     *  object class (empty)
     * \return
     */
    virtual bool check_collision(const object_ptr) override;
protected:
    /*!
     * \brief  The on_update method updates mass of leaf.
     * When mass became is lower <g_leaf_death_mass> leaf will die
     * \param objects vector of scene objects(not used)
     */
    virtual void on_update(const std::vector<object_ptr> &) override;


    /*!
     * \brief The on_collision method overrides abstract method of the
     *  object class (empty)
     * \return
     */
    virtual void on_collision(const object_ptr) override;

    /*!
     * \brief The on_destroy method overrides abstract method of the
     *  object class (empty)
     * \return
     */
    virtual void on_destroy() override;


};
}//namespace b_engine
#endif // LEAF_H
