#include "terminal.h"

void terminal::run_command(const std::string &command)
{
    std::string key;
    size_t pos = command.find(' ');
    if(pos == std::string::npos)
    {
        key = command;
        pos = 0;
    }
    else
    {
        key = command.substr(0, pos);
    }

    auto it = m_commands.find(key);
    if(it == m_commands.end())
    {
        logger::error("Wrong command");
        return;
    }

    it->second.run(command);

    handle_commands();


}

void terminal::help()
{
    std::lock_guard<std::mutex>lk(m_commands_mutex);
    for(auto p: m_commands)
    {
        std::string s;
        s += "\"" + p.first + "\" - " + p.second.descr() + "\n";
        logger::info(s);
    }
}

void terminal::start()
{
    if(m_is_run)
    {
        return;
    }
    m_is_run = true;
    char c;
    std::string buf;
    logger::info("The Terminal is started! Type 'help' for help");
    while(m_is_run)
    {
        std::cin.get(c);
        if (c == 10)
        {
            run_command(buf);
            buf.clear();
            continue;
        }
        buf.push_back(c);

    }
    logger::info("Terminal Stoped!");
}

terminal::terminal()
    : m_commands_queue(
          std::make_shared<multithreading::queue<comm_message::ptr>>())
{
    run_thread();
}

terminal::~terminal()
{
    stop_thread();
    m_thread.join();
}

terminal &terminal::instance()
{
    static terminal me;
    return me;
}

void terminal::handle_commands()
{
    comm_message::ptr command;
    if(!m_commands_queue->try_pop(command))
    {
        return;
    }

    if(command->key == -1)
    {
        help();
    }

}

bool terminal::run_thread()
{
    if(m_is_run)
    {
        return false;
    }

    m_thread = std::thread(std::bind(&terminal::start, this));
    //wait for run
    while(!m_is_run)
    {
        std::this_thread::yield();
    }

    //Adding default comands
    new_command(
                command("help",
                        -1,
                        "help", m_commands_queue));

    return true;
}

void terminal::stop_thread()
{
    m_is_run = false;
}

bool terminal::new_command(const command &comm)
{
    if(!m_is_run)
    {
        throw std::runtime_error("Terminal is not runned!");
    }
    std::lock_guard<std::mutex>lk(m_commands_mutex);
    auto it = m_commands.find(comm.name());
    if(it != m_commands.end())
    {
        logger::error("Terminal:Can't add command! Command " +
                                 comm.name() + " already exists!");
        return false;
    }
    m_commands[comm.name()] = comm;
    return true;
}

size_t terminal::rem_command(const std::string &name)
{
    if(!m_is_run)
    {
        throw std::runtime_error("Terminal is not runned!");
    }
    std::lock_guard<std::mutex>lk(m_commands_mutex);
    return m_commands.erase(name);
}

bool terminal::add_command(const command &comm)
{
    return instance().new_command(comm);
}

bool terminal::remove_command(const std::string &comm_name)
{
    return instance().rem_command(comm_name);
}
