#include "bact_engine/engine.h"
#include "terminal.h"

#include "bact_engine/food_generator.h"
#include "bact_engine/population_controller.h"

#include "multithreading/thread_pool.h"



#include "logger.h"
#include <assert.h>
#include <chrono>
#include <ctime>
#include <sstream>
#include <signal.h>

#define red std::string("\033[31m")
#define gren std::string("\033[32m")
#define yel std::string("\033[33m")
#define blu std::string("\033[34m")
#define clz std::string("\033[0m")


b_engine::popul_param get_pop_param()
{
    b_engine::popul_param p;
    p.brain_struct = {{115 * 4 + 3, activation_type::tanh},
                      {20, activation_type::tanh},
                      {20, activation_type::tanh},
                      {20, activation_type::tanh},
                      {3,  activation_type::tanh}
                     };
    p.population_size = 25;
    p.reccurent = false;
    p.new_gen_mutation_weights_delta = 0.1;
    p.new_gen_mutation_weights_percent = 20;
    p.new_gen_mutation_struct_percent = 15;
    p.new_gen_mutation_struct_opt_param = 0.01;
    p.next_target = 220;
    p.target_multiplier = 1.2;


    std::string data;
    for(const auto & lp:p.brain_struct)
    {
        data +=
                blu + "\t\tlsz:" + red + std::to_string(lp.size)+ "" +
                blu + " typ:" + red + std::to_string(static_cast<int>(lp.type)) + "\n";
    }

    logger::warn(gren + "\n\t\tPSZ:" + red + to_std_str(p.population_size) + "\n" +
                 gren + "\t\tReccurent:" + red + to_std_str((int)p.reccurent) + "\n" +
                 yel + "\tstrct:\n"+ clz + data);

    return p;
}



void sig_handler(int sig)
{
    logger::warn("OS Signal:" + std::to_string(sig) + " Press Enter for exit");
    b_engine::engine::instance().stop();
   // exit(0);
}

int main()
{

    signal(SIGTERM, sig_handler);
    signal(SIGINT, sig_handler);

    srand(static_cast<unsigned int>(std::chrono::duration_cast<std::chrono::seconds>(
                                        std::chrono::system_clock::now().time_since_epoch()).count()));

    logger::init("../logs/log_" + get_time() + ".log");

    using namespace b_engine;

    //ADD FOOD GENERATOR
    std::shared_ptr<food_generator>
            food_gen = std::make_shared<food_generator>();
    engine::instance().
            add_object(food_gen);

    //ADD POPULATION CONTROLLER
    std::shared_ptr<popul_controller> pop_control =
            std::make_shared<popul_controller>(get_pop_param());


    engine::instance().
            add_object(pop_control);

    pop_control->generate();

    engine::instance().run();

    return 0;

}
