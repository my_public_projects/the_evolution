#include "neuro_engine/ann.h"
#include "defines.h"
#include <assert.h>
namespace n_engine {

std::vector<layer> & ann::layers()
{
    return m_layers;
}

void ann::set_input(const std::vector<double> &inputs)
{
    assert(inputs.size() == m_layers[0].size());
    auto it = inputs.begin();
    for(auto & neur : m_layers.front())
    {
        neur.inc_input(*it);
        ++it;
    }
}

void ann::process()
{
    for(auto & layer: m_layers)
    {
        for(auto & neurone: layer)
        {
            neurone.activate();
        }

        if(!m_reccurent) continue;
        for(auto & neurone: layer)
        {
            neurone.reccurent_propagation();
        }
    }
//    if(!m_reccurent) return;

//    for(auto & layer: m_layers)
//    {
//        for(auto & neurone: layer)
//        {
//            neurone.activate(true);
//        }
//    }

}

void ann::get_output(std::vector<double> & out)
{
    assert(out.size() == m_layers.back().size());
    auto it = out.begin();
    for(auto & neur : m_layers.back())
    {
        *it = neur.value();
        ++it;
    }
}

void ann::copy(const ann &r)
{
    const auto & layers = r.m_layers;
    assert(m_layers.size() ==  layers.size());
    auto it = layers.begin();
    for(auto & layer: m_layers)
    {
        assert(it->size() == layer.size());
        auto neur_it = it->begin();
        for(auto & neur : layer)
        {
            neur.copy(*neur_it);
            ++neur_it;
        }
        ++it;
    }
}

void ann::brain_fuck(const ann &parent_one, const ann &parent_two)
{
    assert(parent_one.m_layers.size() == parent_two.m_layers.size() &&
           parent_one.m_layers.size() == m_layers.size());

    const auto & p1_layers = parent_one.m_layers;
    const auto & p2_layers = parent_two.m_layers;

    const size_t my_layers_size = m_layers.size();
    for(size_t li = 0; li < my_layers_size; ++li)
    {
        const size_t curr_layer_s = m_layers[li].size();
        for(size_t ni = 0; ni < curr_layer_s; ++ni)
        {
            m_layers[li][ni].neuro_love(p1_layers[li][ni], p2_layers[li][ni]);
        }
    }


}

void ann::mutate_weights(ulli percent, double delta)
{
    for(auto & layer: m_layers)
    {
        for(auto & neurone: layer)
        {
            neurone.mutate_weights(percent, delta);
        }
    }

}

void ann::mutate_struct(ulli percent, double opt_param)
{
    assert(m_reccurent);
    for(auto & layer: m_layers)
    {
        for(auto & neurone: layer)
        {
            neurone.mutate_struct(percent, opt_param);
        }
    }
}

void ann::serialize(std::string &state) const
{

    for (auto & l :m_layers)
    {
        for(auto & n : l)
        {
            n.serialize(state);
            state+="*";
        }
    }
}

void ann::deserialize(const std::string &state)
{
    std::vector<std::string> neurones;
    slice(neurones, state, '*');
    auto it = neurones.begin();
    for(auto & l: m_layers)
    {
        for (auto & n: l) {
            n.deserialize(*(it++));
        }
    }
}

void ann::init()
{
//    ulli temp = 0;
//    if(!m_reccurent)
//    {
//        for(auto & layer: m_layers)
//            temp += layer.size();
//    }
    //ulli l = 0;
    const auto end = --m_layers.end();
    for(auto l_it = m_layers.begin(); l_it != end; ++l_it)
    {
        auto next_layer = l_it + 1;
        for(auto & neurone: *l_it)
        {
            ulli x = next_layer->size();//TODO may layers overflow
            neurone.init_links(x);
        }
        //++l;
    }
}



ann::ann(const std::vector<layer_param> &layers_params, bool reccurent)
    : m_reccurent(reccurent)
{
    m_layers.resize(layers_params.size());
    m_hidden_first = ++m_layers.begin();
    m_hidden_last = (m_layers.end() - 2);
   // std::vector<layer> & inp_l = m_layers.front()
    ulli layers_count = m_layers.size();
    for(ulli i = 0; i < layers_count; ++i)
    {
        const layer_param & param = layers_params[i];
        for(ulli j = 0; j < param.size; ++j)
        {
            m_layers[i].push_back(neurone(this, m_reccurent, j, i, param.type));
        }
    }
    init();
}



} // namespace n_engine
