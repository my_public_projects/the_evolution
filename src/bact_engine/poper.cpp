#include "bact_engine/poper.h"
#include "bact_engine/particles.h"
#include "math.h"

namespace b_engine {

std::shared_ptr<particle> poper::make(float r)
{
    float t = ((std::rand() % 2001)-1000);
    float temp_t = (t/1000);
    float rad = r * temp_t;
    float sin = std::sin(2 * g_pi + t);
    float cos = std::cos(2 * g_pi + t);
    float s_x = sin*2*temp_t;
    float s_y = cos*2*temp_t;
    float _x = x() + sin * rad;
    float _y = y() + cos * rad;
    auto res = std::make_shared<particle>(s_x, s_y, sf::Color(220,0, 255));
    res->set_pos(_x, _y);
    return res;
}

void poper::generate(float radius)
{
    for(int i = 0; i < 120; ++i)
    {
        m_particles.push_back(make(radius));
    }
}

poper::poper(float radius, float x, float y)
    : object (object_type::object)
{
    set_pos(x,y);
    generate(radius);
}

poper::~poper()
{

}

void poper::draw(sf::RenderWindow &window)
{
    for(const auto & p:m_particles)
    {
        p->draw(window);
    }
}

bool poper::check_collision(const object_ptr )
{
    return false;
}

void poper::on_update(const std::vector<object_ptr> &)
{
    int live = 0;
    for(auto & p:m_particles)
    {
        p->update(m_particles);
        if(!p->need_remove())
            live++;
    }
    if(!live) destroy();
}

void poper::on_collision(const object_ptr )
{

}

void poper::on_destroy()
{

}

}
