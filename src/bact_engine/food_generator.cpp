#include "bact_engine/food_generator.h"
#include "bact_engine/engine.h"

namespace b_engine
{


bool food_generator::find_place(const food_generator::leaf_ptr &_food, const std::vector<object_ptr> &objs)
{
    int trys_count = 10;
    int result = true;
    while(trys_count)
    {
        result = true;

        for(const auto & o: objs)
        {

            if(!o || o->type() != object_type::bact)
            {
                continue;
            }
            if(_food->is_collide(o))
            {
                result = false;
                break;
            }
        }

        if(result)
        {
            return true;
        }
        _food->set_pos(rand() % static_cast<int>(g_world_width),
                       rand() % static_cast<int>(g_world_height));
        trys_count--;
    }
    return result;
}


bool food_generator::respawn(ull index)
{
    auto _new = std::make_shared<leaf>();
    const std::vector<object_ptr> & objs = engine::instance().objects();
    if(!find_place(_new, objs))
    {
        logger::warn("Can't find empty place for food!");
        return false;
    }
    m_food[index] = _new;
    engine::instance().add_object(_new);
    return  true;
}


void food_generator::generate()
{
    ull delta = g_leaf_number - m_food.size();
    for(ull i = 0; i < delta; ++i)
    {
        m_food.push_back(std::make_shared<leaf>());
        engine::instance().add_object(m_food.back());
    }
}


food_generator::food_generator()
    : object(object_type::object)
{
    generate();
}


void food_generator::update()
{
    generate();
    ull size = m_food.size();
    for(ull i = 0; i < size; ++i)
    {
        if(m_food[i]->is_destroy())
        {
            if(!respawn(i))
            {
                logger::warn("Warning: Can't respawn food!");
            }
        }
    }
}



}// namespace b_engine
