#include "bact_engine/object.h"
#include <assert.h>
namespace b_engine {
object_type object::type() const
{
    return m_type;
}

float object::radius() const
{
    return m_radius;
}

float object::mass() const
{
    return m_mass;
}

void object::set_mass(float mass)
{
    // r = sqrt (mass/density/pi) ;
    assert(mass != 0);
    m_mass = mass;
    m_radius = std::sqrt(m_mass/g_density/g_pi);
    assert(!std::isnan(m_radius));
}

void object::delta_mass(float delta)
{
    float m = m_mass + delta;
    m = m <= 0? 0.1: m;
    set_mass(m);
}

void object::destroy()
{
    m_need_remove = true;
    on_destroy();
}

void object::respawn()
{
    m_is_destroy = false;
    m_need_remove = false;
}


bool object::is_destroy() const
{
    return m_is_destroy;
}

void object::set_color(const sf::Color &color)
{
    m_color = color;
}

const sf::Color &object::get_color() const
{
    return m_color;
}

object::object(object_type type)
    : m_type(type)
{

}

float object::y() const
{
    return m_y;
}

float object::x() const
{
    return m_x;
}

void object::set_x(float x)
{
    m_x = x;
}

void object::set_y(float y)
{
    m_y = y;
}

void object::set_pos(float x, float y)
{
    assert(!(std::isnan(x) || std::isnan(y)));
    m_x = x;
    m_y = y;
}

void object::update(const std::vector<object_ptr> & environment)
{
    on_update(environment);
}

object::~object()
{
}

sf::Color get_rand_color()
{
    return sf::Color(std::rand() % 256, std::rand() % 256, std::rand() % 256);
}

void mutate_color(sf::Color &color, [[maybe_unused]]double delta)
{
    sf::Uint8 * c_v = nullptr;
    switch (rand() % 3) {
    case 0 :
        c_v = &color.r;
        break;
    case 1 :
        c_v = &color.g;
        break;
    case 2 :
        c_v = &color.b;
    }

    double random = double(rand() % 2001 - 1000) / 1000;
    random = random > 0 ? 1. : -1;
    int res = *c_v + /*(255 * delta) **/ random;
    res = res > 255 ? 255 : res < 0 ? 0 : res;
    *c_v = res;
}

}// namespace b_engine
