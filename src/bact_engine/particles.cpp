#include "bact_engine/particles.h"

namespace b_engine
{

void particle::init()
{
    m_circle.setRadius(2);
    m_circle.setFillColor(sf::Color(12, 180,180, 90));
    m_circle.setOrigin(1,1);
}

particle::particle(float v_x, float v_y)
    :object (object_type::object),
     m_speed(v_x, v_y)
{
    init();
}

particle::particle(float v_x,
                   float v_y,
                   const sf::Color &color)
    :object (object_type::object),
      m_speed(v_x, v_y)
{
    m_circle.setRadius(2);
    m_circle.setFillColor(color);
    m_circle.setOrigin(1,1);
}

particle::~particle()
{

}

void particle::draw(sf::RenderWindow &window)
{
    m_circle.setPosition(x(), y());
    window.draw(m_circle);
}

void particle::on_update(const std::vector<object_ptr> &)
{
    m_life--;
    m_speed *= g_friction;
    set_pos(x() + m_speed.x, y() + m_speed.y);
    if(m_life < 0)
    {
        destroy();
    }
}

bool particle::check_collision(const object_ptr )
{
    return false;
}

void particle::on_collision(const object_ptr )
{

}

void particle::on_destroy()
{

}

}//namespace b_engine
