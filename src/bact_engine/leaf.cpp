#include "bact_engine/leaf.h"

namespace b_engine {

leaf::leaf()
    : object (object_type::leaf)
{
    set_mass( g_leaf_min_born_mass + rand() % static_cast<int>(g_leaf_max_born_mass));
    set_pos(rand() % static_cast<int>(g_world_width),
            rand() % static_cast<int>(g_world_height));
    set_color(sf::Color(0,255,0));
}

leaf::~leaf()
{

}

void leaf::on_update(const std::vector<object_ptr> &)
{
    delta_mass(g_leaf_mass_burn_out);
    if(mass() < g_leaf_death_mass)
    {
        destroy();
    }
}

bool leaf::check_collision(const object_ptr)
{
    return false;
}

void leaf::on_collision(const object_ptr)
{

}

void leaf::on_destroy()
{

}

void leaf::draw(sf::RenderWindow &window)
{
    sf::CircleShape c;
    c.setFillColor(get_color());
    float r = radius();
    c.setRadius(r);
    c.setPosition(x(), y());
    c.setOrigin(r,r);
    window.draw(c);
}

}//namespace b_engine
