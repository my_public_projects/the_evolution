#include "defines.h"

void slice(std::vector<std::string> &out, const std::string &in, char separator)
{

    ull pos = 0;
    ull cur = 0;
    while(cur != std::string::npos)
    {
        cur = in.find(separator, pos);
        out.push_back(in.substr(pos, cur-pos));
        pos = cur+1;
    }

}


std::string get_time()
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t t = std::chrono::system_clock::to_time_t(now);
    std::string st = std::ctime(&t);
    for(auto & i : st)
    {
        if(i == ' ' || i == '\n')
        {
            i = '_';
        }
    }
    return st;
}
