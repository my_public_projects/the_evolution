#include <iostream>
#include <assert.h>
#include <functional>

#include "logger.h"
#include <cxxabi.h>
#include <sstream>



logger::logger()
{

}

logger::~logger()
{
    m_run = false;
    m_thread.join();
    info("Logger finished log file: " + m_log_filename);
    while(m_messages.size())
    {
        std::shared_ptr<message_queue::message> mess = m_messages.pop();
        print(mess);
    }
    m_fstream.close();
}

void logger::print(const std::shared_ptr<message_queue::message> &mess)
{
    std::stringstream ss;
    ss << mess->m_thread_id;
    write( ss.str() + ":\033[33m" + time_to_string(mess->m_stamp) + "\033[0m    " + mess->m_data);
    //static std::time_t last_time = 0;
//    if(mess->m_stamp != last_time)
//    {
//        last_time = mess->m_stamp;
//        write("\033[33m" + time_to_string(last_time) + "\033[0m\n    " + mess->m_data);
//    } else {
//        write("    " + mess->m_data);
//    }
}

void logger::write(const std::string &data)
{
    std::cout << data;

    m_fstream << data;
}

void logger::process()
{
    while(m_run)
    {
        while(m_messages.size())
        {
            std::shared_ptr<message_queue::message> mess = m_messages.pop();
            print(mess);
        }
        m_fstream.sync();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

logger &logger::instance()
{
    static logger me;
    return me;
}

void logger::init_pvt(const std::string &log_file_name)
{
    m_log_filename = log_file_name;
    m_fstream.open(m_log_filename, std::fstream::out);
    if(!m_fstream.is_open()) {
        throw std::fstream::failure("Can't open file to write" + log_file_name);
    }

    m_thread = std::thread(std::bind(&logger::process, this));

    info("Logger inited! log file" + log_file_name);

}


void logger::init(const std::string &log_file_name)
{
    instance().init_pvt(log_file_name);
}

std::shared_ptr<message_queue::message> make_mess(const std::string & mes)
{
    return std::make_shared<message_queue::message>(mes);
}

std::shared_ptr<message_queue::message> make_mess(std::string && mes)
{

    return std::make_shared<message_queue::message >(mes);
}

void logger::info(const std::string &mess, bool new_line)
{
    instance().m_messages.push(make_mess(l_str + "\033[32mI:" + mess + "\033[0m" + (new_line ? "\n" :"")));
}

void logger::info(std::string &&mess, bool new_line)
{
    instance().m_messages.push(make_mess(l_str + "\033[32mI:" + mess + "\033[0m" +  (new_line ? "\n": "")));
}

void logger::warn(const std::string &mess, bool new_line)
{
    instance().m_messages.push(make_mess(l_str + "\033[33mW:" + mess + "\033[0m" + (new_line ? "\n": "")));
}

void logger::warn(const std::string &&mess, bool new_line)
{
    instance().m_messages.push(make_mess(l_str + "\033[33mW:" + mess + "\033[0m" +  (new_line ? "\n": "")));
}

void logger::error(const std::string &mess, bool new_line)
{
    instance().m_messages.push(make_mess(l_str + "\033[31mERROR:" + mess + "\033[0m" + (new_line ? "\n": "")));
}

void logger::error(const std::string &&mess, bool new_line)
{
     instance().m_messages.push(make_mess(l_str + "\033[31mERROR:" + mess + "\033[0m" + (new_line ? "\n": "")));
}

std::string logger::time_to_string(time_t t)
{
    char buff[30];
    struct tm * time = std::localtime(&t);
    if(!std::strftime(buff, 30, "%d-%m-%y_%H:%M:%S", time))
    {
        return "time ERROR!";
    }
    return std::string(buff);
}





/// ---- message_queue----
message_queue::message_queue()
{

}

void message_queue::push(const std::shared_ptr<message> &_message)
{
    std::lock_guard<std::mutex> lk(m_mutex);

    m_queue.push(_message);
}

std::shared_ptr<message_queue::message> message_queue::pop()
{
    std::lock_guard<std::mutex> lk(m_mutex);
    std::shared_ptr<message> res = m_queue.front();
    m_queue.pop();
    return res;
}

size_t message_queue::size()
{
    std::lock_guard<std::mutex> lk(m_mutex);
    return m_queue.size();
}

std::string demangle(const char *name)
{
    int status = -4; // some arbitrary value to eliminate the compiler warning

        // enable c++11 by passing the flag -std=c++11 to g++
        std::unique_ptr<char, void(*)(void*)> res {
            abi::__cxa_demangle(name, nullptr, nullptr, &status),
            std::free
        };

        return (status==0) ? res.get() : name ;
}
