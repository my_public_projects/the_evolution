#include "multithreading/thread_pool.h"
namespace multithreading{

void thread_pool::worker()
{
    while(m_is_working)
    {
        job temp_job;
        if(m_intense)
        {
            if(!m_jobs_q.try_pop(temp_job))
            {
                std::this_thread::yield();
                continue;
            }
        }
        else
        {
            m_jobs_q.wait_pop(temp_job);
        }

        try {
            temp_job->operator()();
        }
        catch (const std::bad_function_call & e)
        {
            std::cout << "multithreading::thread_pool::woker()->"
                         " std::bad_function_call:" <<
                         e.what();
            throw e;
        }
    }
}

void thread_pool::stop_threads()
{
    m_is_working = false;
    for(const auto & t:m_threads)
    {
        (void)t;//wake up, sunny
        m_jobs_q.push(std::make_shared<std::function<void()>>([](){}));
    }

    for(auto & t: m_threads)
    {
        t.join();
    }
}

thread_pool::thread_pool(size_t thread_count, bool intense, size_t max_queue_size)
    : m_threads_count((thread_count ?
                           thread_count :
                           std::thread::hardware_concurrency())),
      m_is_working(true),
      m_intense(intense)
{
    if(max_queue_size)
        m_jobs_q.set_max_size(max_queue_size);

    for(size_t i=0; i < m_threads_count; ++i)
    {
        m_threads.push_back(
                    std::thread(
                        std::bind(&thread_pool::worker, this)
                        )
                    );
    }
}

thread_pool::~thread_pool()
{
    stop_threads();
}

bool thread_pool::submit(const std::function<void ()> &job)
{
    return m_jobs_q.push(std::make_shared<std::function<void()>>(job));
}

bool thread_pool::submit(std::function<void ()> &&job)
{
    return m_jobs_q.push(std::make_shared<std::function<void()>>(std::move(job)));
}

bool thread_pool::submit(const thread_pool::job &job)
{
    return m_jobs_q.push(job);
}

size_t thread_pool::get_threads_count() const
{
    return m_threads.size();
}

size_t thread_pool::get_max_queue_size() const
{
    return m_jobs_q.max_size();
}

}
