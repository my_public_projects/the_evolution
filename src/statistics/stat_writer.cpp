#include "statistics/stat_writer.h"
#include "logger.h"
#include <assert.h>

stat_writer::stat_writer(const std::string &file_name)
    : m_fstream(file_name, std::fstream::out)
{
    if(!m_fstream.is_open())
    {
        printf("error:%s%s", __FUNCTION__, "I/O \033[32mЕггог!\033[0m");
        assert(false);
    }
    logger::info("Statistics writing to:" + file_name);
}

stat_writer::~stat_writer()
{
    m_fstream.close();
}

void stat_writer::write(std::list<std::shared_ptr<stat_frame> > &data, size_t count)
{

    while(data.size() > count)
    {
        m_fstream << data.front()->serialize() << "\n";
        data.pop_front();
    }
}
