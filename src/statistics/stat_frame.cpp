#include <iomanip>
#include <sstream>

#include "statistics/stat_frame.h"
#include "bact_engine/engine.h"

stat_frame::stat_frame()
    : m_data(std::make_shared<data_map>()),
      m_time_stamp(b_engine::engine::instance().ticks_couner())
{

}


stat_frame::stat_frame(const std::vector<std::pair<std::string, double>> & params)
    : m_data(std::make_shared<data_map>()),
      m_time_stamp(b_engine::engine::instance().ticks_couner())
{
    for(const auto & p:params)
    {
        m_data->insert(p);
    }
}

void stat_frame::add_param(const std::string &param_name, double param_value)
{
    (*m_data)[param_name] = param_value;
}

std::shared_ptr<const data_map> stat_frame::get_data() const
{
    return m_data;
}

std::size_t stat_frame::get_tick_stamp() const
{
    return m_time_stamp;
}

std::string stat_frame::serialize() const
{
    std::stringstream ss;
    ss << std::setprecision(16);
    ss << m_time_stamp << ";";
    for(auto const & p:*m_data)
    {
        ss << p.first << ":" << p.second << ";";
    }
    return ss.str();
}

void stat_frame::deserialize(const std::string &data)
{
    std::vector<std::string> out;
    slice(out, data, ';');
    out.pop_back();
    auto it = out.begin();
    m_time_stamp = std::stoull(*it);
    ++it;
    while(it != out.end())
    {
        std::vector<std::string> param;
        slice(param, *it, ':');
        add_param(param[0], std::stod(param[1]));
        ++it;
    }
}
