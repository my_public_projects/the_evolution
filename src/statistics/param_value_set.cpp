#include "statistics/param_value_set.h"

param_value_set::param_value_set()
{

}

param_value_set::param_value_set(const std::string &param_name)
    : m_name(param_name)
{

}

double param_value_set::mean()
{
    double sum = 0.0;
    for (auto it = begin(); it != end(); ++it)
    {
        sum += (*it)->get_data()->at(m_name);
    }
    return sum / size();
}

const std::string &param_value_set::name() const
{
    return m_name;
}

void param_value_set::set_name(const std::string &name)
{
    m_name = name;
}
