#include "statistics/statistic_holder.h"

#include "bact_engine/engine.h"

void stat_holder::capture_primary()
{
    std::lock_guard<std::mutex> lk(m_data_frames_mutex);
    m_data_frames = m_secondary_list;
}

void stat_holder::parse_frame(const std::shared_ptr<stat_frame> &frame)
{
    auto data = frame->get_data();

    for (auto it = data->begin(); it != data->end(); ++it) {
        auto & p_set = m_param_sets[it->first];
        p_set.set_name(it->first);
        p_set.push_back(frame);
    }

}

void stat_holder::reparse_sets()
{
    std::lock_guard<std::mutex> lk(m_param_set_mutex);
    m_param_sets.clear();
    for(auto it = m_primary_list->begin(); it != m_primary_list->end(); ++it)
    {
        parse_frame(*it);
    }
}

void stat_holder::push_data(const std::shared_ptr<stat_frame> &frame)
{
    m_data_frames->push_back(frame);
    if(m_data_frames != m_primary_list)
    {
        return;
    }
    parse_frame(frame);
}

void stat_holder::attach_secondary()
{
    while(m_secondary_list->size())
    {
        m_primary_list->push_back(m_secondary_list->front());
        parse_frame(m_secondary_list->front());
        m_secondary_list->pop_front();
    }
}

void stat_holder::relize_primary()
{
    std::lock_guard<std::mutex> lk(m_data_frames_mutex);
    if(m_secondary_list->size() != 0)
    {
        attach_secondary();
    }
    m_data_frames = m_primary_list;
}

void stat_holder::worker()
{
    while(m_is_not_join)
    {

        if(m_primary_list->size() > m_max_size)
        {
            size_t size = static_cast<size_t>(m_primary_list->size() * 0.8);
            write(size);
        }

        // CamelCaseIsThis - верблюжий not_camel_case - не верблюжий)))


        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    write(0);
}

void stat_holder::write(size_t count)
{
    //logger::warn("Uploading statistics...");
    capture_primary();
    m_writer.write(*m_primary_list, count);
    reparse_sets();
    relize_primary();
    //logger::warn(" The statistics is uploaded.");
}

stat_holder::stat_holder(const std::string &out_file_name)
    : m_primary_list(std::make_shared<std::list<std::shared_ptr<stat_frame>>>()),
      m_secondary_list(std::make_shared<std::list<std::shared_ptr<stat_frame>>>()),
      m_writer(out_file_name)
{
    relize_primary();
    m_calculator_thread = std::thread(std::bind(&stat_holder::worker, this));
}

stat_holder::~stat_holder()
{
    m_is_not_join = false;
    m_calculator_thread.join();
}

void stat_holder::push(const stat_frame &frame)
{
    std::lock_guard<std::mutex> lk(m_data_frames_mutex);
    ++m_push_counter;
    push_data(std::make_shared<stat_frame>(frame));
    //m_data_frames->push_back(frame);
}

double stat_holder::mean(const std::string &param_name)
{
    std::lock_guard<std::mutex> lk(m_data_frames_mutex);
    std::lock_guard<std::mutex> lk2(m_param_set_mutex);
    auto it = m_param_sets.find(param_name);
    if(it == m_param_sets.end())
    {
        logger::error(l_str + __FUNCTION__ + "==> param '" + param_name + "' not exists");
        return 0.0;
    }

    double res = it->second.size() > 0 ? it->second.mean() : 0;
    return res;

}
